//Sqlc generated V1.O00-1
package com.atrums.balance.evolutivo;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

public class AccountTreeData implements FieldProvider {
static Logger log4j = Logger.getLogger(AccountTreeData.class);
  private String InitRecordNumber="0";
  public String nodeId;
  public String parentId;
  public String seqno;
  public String id;
  public String name;
  public String description;
  public String issummary;
  public String accountsign;
  public String showelement;
  public String elementLevel;
  public String qty;
  public String qtyMes1;
  public String qtyMes2;
  public String qtyMes3;
  public String qtyMes4;
  public String qtyMes5;
  public String qtyMes6;
  public String qtyMes7;
  public String qtyMes8;
  public String qtyMes9;
  public String qtyMes10;
  public String qtyMes11;
  public String qtyMes12;
  public String qtyRef;
  public String qtyOperation;
  public String qtyOperationRef;
  public String qtyOperationMes1;
  public String qtyOperationMes2;
  public String qtyOperationMes3;
  public String qtyOperationMes4;
  public String qtyOperationMes5;
  public String qtyOperationMes6;
  public String qtyOperationMes7;
  public String qtyOperationMes8;
  public String qtyOperationMes9;
  public String qtyOperationMes10;
  public String qtyOperationMes11;
  public String qtyOperationMes12;
  public String qtycredit;
  public String qtycreditRef;
  public String qtycreditMes1;
  public String qtycreditMes2;
  public String qtycreditMes3;
  public String qtycreditMes4;
  public String qtycreditMes5;
  public String qtycreditMes6;
  public String qtycreditMes7;
  public String qtycreditMes8;
  public String qtycreditMes9;
  public String qtycreditMes10;
  public String qtycreditMes11;
  public String qtycreditMes12;
  public String showvaluecond;
  public String elementlevel;
  public String value;
  public String calculated;
  public String svcreset;
  public String svcresetref;
  public String svcresetmes1;
  public String svcresetmes2;
  public String svcresetmes3;
  public String svcresetmes4;
  public String svcresetmes5;
  public String svcresetmes6;
  public String svcresetmes7;
  public String svcresetmes8;
  public String svcresetmes9;
  public String svcresetmes10;
  public String svcresetmes11;
  public String svcresetmes12;
  public String isalwaysshown;
  public String sign;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("node_id") || fieldName.equals("nodeId"))
      return nodeId;
    else if (fieldName.equalsIgnoreCase("parent_id") || fieldName.equals("parentId"))
      return parentId;
    else if (fieldName.equalsIgnoreCase("seqno"))
      return seqno;
    else if (fieldName.equalsIgnoreCase("id"))
      return id;
    else if (fieldName.equalsIgnoreCase("name"))
      return name;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("issummary"))
      return issummary;
    else if (fieldName.equalsIgnoreCase("accountsign"))
      return accountsign;
    else if (fieldName.equalsIgnoreCase("showelement"))
      return showelement;
    else if (fieldName.equalsIgnoreCase("element_level") || fieldName.equals("elementLevel"))
      return elementLevel;
    else if (fieldName.equalsIgnoreCase("qty"))
      return qty;
    else if (fieldName.equalsIgnoreCase("qty_mes1") || fieldName.equals("qtyMes1"))
      return qtyMes1;
    else if (fieldName.equalsIgnoreCase("qty_mes2") || fieldName.equals("qtyMes2"))
      return qtyMes2;
    else if (fieldName.equalsIgnoreCase("qty_mes3") || fieldName.equals("qtyMes3"))
      return qtyMes3;
    else if (fieldName.equalsIgnoreCase("qty_mes4") || fieldName.equals("qtyMes4"))
      return qtyMes4;
    else if (fieldName.equalsIgnoreCase("qty_mes5") || fieldName.equals("qtyMes5"))
      return qtyMes5;
    else if (fieldName.equalsIgnoreCase("qty_mes6") || fieldName.equals("qtyMes6"))
      return qtyMes6;
    else if (fieldName.equalsIgnoreCase("qty_mes7") || fieldName.equals("qtyMes7"))
      return qtyMes7;
    else if (fieldName.equalsIgnoreCase("qty_mes8") || fieldName.equals("qtyMes8"))
      return qtyMes8;
    else if (fieldName.equalsIgnoreCase("qty_mes9") || fieldName.equals("qtyMes9"))
      return qtyMes9;
    else if (fieldName.equalsIgnoreCase("qty_mes10") || fieldName.equals("qtyMes10"))
      return qtyMes10;
    else if (fieldName.equalsIgnoreCase("qty_mes11") || fieldName.equals("qtyMes11"))
      return qtyMes11;
    else if (fieldName.equalsIgnoreCase("qty_mes12") || fieldName.equals("qtyMes12"))
      return qtyMes12;
    else if (fieldName.equalsIgnoreCase("qty_ref") || fieldName.equals("qtyRef"))
      return qtyRef;
    else if (fieldName.equalsIgnoreCase("qty_operation") || fieldName.equals("qtyOperation"))
      return qtyOperation;
    else if (fieldName.equalsIgnoreCase("qty_operation_ref") || fieldName.equals("qtyOperationRef"))
      return qtyOperationRef;
    else if (fieldName.equalsIgnoreCase("qty_operation_mes1") || fieldName.equals("qtyOperationMes1"))
      return qtyOperationMes1;
    else if (fieldName.equalsIgnoreCase("qty_operation_mes2") || fieldName.equals("qtyOperationMes2"))
      return qtyOperationMes2;
    else if (fieldName.equalsIgnoreCase("qty_operation_mes3") || fieldName.equals("qtyOperationMes3"))
      return qtyOperationMes3;
    else if (fieldName.equalsIgnoreCase("qty_operation_mes4") || fieldName.equals("qtyOperationMes4"))
      return qtyOperationMes4;
    else if (fieldName.equalsIgnoreCase("qty_operation_mes5") || fieldName.equals("qtyOperationMes5"))
      return qtyOperationMes5;
    else if (fieldName.equalsIgnoreCase("qty_operation_mes6") || fieldName.equals("qtyOperationMes6"))
      return qtyOperationMes6;
    else if (fieldName.equalsIgnoreCase("qty_operation_mes7") || fieldName.equals("qtyOperationMes7"))
      return qtyOperationMes7;
    else if (fieldName.equalsIgnoreCase("qty_operation_mes8") || fieldName.equals("qtyOperationMes8"))
      return qtyOperationMes8;
    else if (fieldName.equalsIgnoreCase("qty_operation_mes9") || fieldName.equals("qtyOperationMes9"))
      return qtyOperationMes9;
    else if (fieldName.equalsIgnoreCase("qty_operation_mes10") || fieldName.equals("qtyOperationMes10"))
      return qtyOperationMes10;
    else if (fieldName.equalsIgnoreCase("qty_operation_mes11") || fieldName.equals("qtyOperationMes11"))
      return qtyOperationMes11;
    else if (fieldName.equalsIgnoreCase("qty_operation_mes12") || fieldName.equals("qtyOperationMes12"))
      return qtyOperationMes12;
    else if (fieldName.equalsIgnoreCase("qtycredit"))
      return qtycredit;
    else if (fieldName.equalsIgnoreCase("qtycredit_ref") || fieldName.equals("qtycreditRef"))
      return qtycreditRef;
    else if (fieldName.equalsIgnoreCase("qtycredit_mes1") || fieldName.equals("qtycreditMes1"))
      return qtycreditMes1;
    else if (fieldName.equalsIgnoreCase("qtycredit_mes2") || fieldName.equals("qtycreditMes2"))
      return qtycreditMes2;
    else if (fieldName.equalsIgnoreCase("qtycredit_mes3") || fieldName.equals("qtycreditMes3"))
      return qtycreditMes3;
    else if (fieldName.equalsIgnoreCase("qtycredit_mes4") || fieldName.equals("qtycreditMes4"))
      return qtycreditMes4;
    else if (fieldName.equalsIgnoreCase("qtycredit_mes5") || fieldName.equals("qtycreditMes5"))
      return qtycreditMes5;
    else if (fieldName.equalsIgnoreCase("qtycredit_mes6") || fieldName.equals("qtycreditMes6"))
      return qtycreditMes6;
    else if (fieldName.equalsIgnoreCase("qtycredit_mes7") || fieldName.equals("qtycreditMes7"))
      return qtycreditMes7;
    else if (fieldName.equalsIgnoreCase("qtycredit_mes8") || fieldName.equals("qtycreditMes8"))
      return qtycreditMes8;
    else if (fieldName.equalsIgnoreCase("qtycredit_mes9") || fieldName.equals("qtycreditMes9"))
      return qtycreditMes9;
    else if (fieldName.equalsIgnoreCase("qtycredit_mes10") || fieldName.equals("qtycreditMes10"))
      return qtycreditMes10;
    else if (fieldName.equalsIgnoreCase("qtycredit_mes11") || fieldName.equals("qtycreditMes11"))
      return qtycreditMes11;
    else if (fieldName.equalsIgnoreCase("qtycredit_mes12") || fieldName.equals("qtycreditMes12"))
      return qtycreditMes12;
    else if (fieldName.equalsIgnoreCase("showvaluecond"))
      return showvaluecond;
    else if (fieldName.equalsIgnoreCase("elementlevel"))
      return elementlevel;
    else if (fieldName.equalsIgnoreCase("value"))
      return value;
    else if (fieldName.equalsIgnoreCase("calculated"))
      return calculated;
    else if (fieldName.equalsIgnoreCase("svcreset"))
      return svcreset;
    else if (fieldName.equalsIgnoreCase("svcresetref"))
      return svcresetref;
    else if (fieldName.equalsIgnoreCase("svcresetmes1"))
      return svcresetmes1;
    else if (fieldName.equalsIgnoreCase("svcresetmes2"))
      return svcresetmes2;
    else if (fieldName.equalsIgnoreCase("svcresetmes3"))
      return svcresetmes3;
    else if (fieldName.equalsIgnoreCase("svcresetmes4"))
      return svcresetmes4;
    else if (fieldName.equalsIgnoreCase("svcresetmes5"))
      return svcresetmes5;
    else if (fieldName.equalsIgnoreCase("svcresetmes6"))
      return svcresetmes6;
    else if (fieldName.equalsIgnoreCase("svcresetmes7"))
      return svcresetmes7;
    else if (fieldName.equalsIgnoreCase("svcresetmes8"))
      return svcresetmes8;
    else if (fieldName.equalsIgnoreCase("svcresetmes9"))
      return svcresetmes9;
    else if (fieldName.equalsIgnoreCase("svcresetmes10"))
      return svcresetmes10;
    else if (fieldName.equalsIgnoreCase("svcresetmes11"))
      return svcresetmes11;
    else if (fieldName.equalsIgnoreCase("svcresetmes12"))
      return svcresetmes12;
    else if (fieldName.equalsIgnoreCase("isalwaysshown"))
      return isalwaysshown;
    else if (fieldName.equalsIgnoreCase("sign"))
      return sign;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static AccountTreeData[] select(ConnectionProvider connectionProvider, String conCodigo, String adTreeId)    throws ServletException {
    return select(connectionProvider, conCodigo, adTreeId, 0, 0);
  }

  public static AccountTreeData[] select(ConnectionProvider connectionProvider, String conCodigo, String adTreeId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT tn.Node_ID,tn.Parent_ID,tn.SeqNo, m.C_ElementValue_ID AS ID, " +
      "        ((CASE ? WHEN 'Y' THEN TO_CHAR(m.Value) || ' - ' ELSE '' END) || m.Name) AS NAME,m.Description, m.IsSummary, " +
      "        m.ACCOUNTSIGN, m.ShowElement, 0 as element_level, 0 as qty, " +
      "        0 AS qty_mes1, " +
      "        0 AS qty_mes2, " +
      "        0 AS qty_mes3, " +
      "        0 AS qty_mes4, " +
      "        0 AS qty_mes5, " +
      "        0 AS qty_mes6, " +
      "        0 AS qty_mes7, " +
      "        0 AS qty_mes8, " +
      "        0 AS qty_mes9," +
      "        0 AS qty_mes10," +
      "        0 AS qty_mes11, " +
      "        0 AS qty_mes12," +
      "        0 as qty_ref, 0 as qty_operation, 0 as qty_operation_ref, " +
      "        0 as qty_operation_mes1," +
      "        0 as qty_operation_mes2," +
      "        0 as qty_operation_mes3," +
      "        0 as qty_operation_mes4," +
      "        0 as qty_operation_mes5," +
      "        0 as qty_operation_mes6," +
      "        0 as qty_operation_mes7," +
      "        0 as qty_operation_mes8," +
      "        0 as qty_operation_mes9," +
      "        0 as qty_operation_mes10," +
      "        0 as qty_operation_mes11," +
      "        0 as qty_operation_mes12," +
      "        0 as QTYCREDIT, 0 as QTYCREDIT_REF, " +
      "        0 as QTYCREDIT_MES1," +
      "        0 as QTYCREDIT_MES2," +
      "        0 as QTYCREDIT_MES3," +
      "        0 as QTYCREDIT_MES4," +
      "        0 as QTYCREDIT_MES5," +
      "        0 as QTYCREDIT_MES6," +
      "        0 as QTYCREDIT_MES7," +
      "        0 as QTYCREDIT_MES8," +
      "        0 as QTYCREDIT_MES9," +
      "        0 as QTYCREDIT_MES10," +
      "        0 as QTYCREDIT_MES11," +
      "        0 as QTYCREDIT_MES12," +
      "        m.ShowValueCond, m.ElementLevel, m.Value, " +
      "        'N' AS CALCULATED, 'N' AS SVCRESET, 'N' AS SVCRESETREF, " +
      "        'N' AS SVCRESETMES1," +
      "        'N' AS SVCRESETMES2," +
      "        'N' AS SVCRESETMES3," +
      "        'N' AS SVCRESETMES4, " +
      "        'N' AS SVCRESETMES5," +
      "        'N' AS SVCRESETMES6," +
      "        'N' AS SVCRESETMES7," +
      "        'N' AS SVCRESETMES8," +
      "        'N' AS SVCRESETMES9," +
      "        'N' AS SVCRESETMES10," +
      "        'N' AS SVCRESETMES11," +
      "        'N' AS SVCRESETMES12," +
      "        m.isalwaysshown, '' as sign" +
      "        FROM AD_TreeNode tn, C_ElementValue m" +
      "        WHERE tn.IsActive='Y' " +
      "        AND tn.Node_ID = m.C_ElementValue_ID " +
      "        AND tn.AD_Tree_ID = ? " +
      "        ORDER BY COALESCE(tn.Parent_ID, '-1'), tn.SeqNo";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, conCodigo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adTreeId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AccountTreeData objectAccountTreeData = new AccountTreeData();
        objectAccountTreeData.nodeId = UtilSql.getValue(result, "node_id");
        objectAccountTreeData.parentId = UtilSql.getValue(result, "parent_id");
        objectAccountTreeData.seqno = UtilSql.getValue(result, "seqno");
        objectAccountTreeData.id = UtilSql.getValue(result, "id");
        objectAccountTreeData.name = UtilSql.getValue(result, "name");
        objectAccountTreeData.description = UtilSql.getValue(result, "description");
        objectAccountTreeData.issummary = UtilSql.getValue(result, "issummary");
        objectAccountTreeData.accountsign = UtilSql.getValue(result, "accountsign");
        objectAccountTreeData.showelement = UtilSql.getValue(result, "showelement");
        objectAccountTreeData.elementLevel = UtilSql.getValue(result, "element_level");
        objectAccountTreeData.qty = UtilSql.getValue(result, "qty");
        objectAccountTreeData.qtyMes1 = UtilSql.getValue(result, "qty_mes1");
        objectAccountTreeData.qtyMes2 = UtilSql.getValue(result, "qty_mes2");
        objectAccountTreeData.qtyMes3 = UtilSql.getValue(result, "qty_mes3");
        objectAccountTreeData.qtyMes4 = UtilSql.getValue(result, "qty_mes4");
        objectAccountTreeData.qtyMes5 = UtilSql.getValue(result, "qty_mes5");
        objectAccountTreeData.qtyMes6 = UtilSql.getValue(result, "qty_mes6");
        objectAccountTreeData.qtyMes7 = UtilSql.getValue(result, "qty_mes7");
        objectAccountTreeData.qtyMes8 = UtilSql.getValue(result, "qty_mes8");
        objectAccountTreeData.qtyMes9 = UtilSql.getValue(result, "qty_mes9");
        objectAccountTreeData.qtyMes10 = UtilSql.getValue(result, "qty_mes10");
        objectAccountTreeData.qtyMes11 = UtilSql.getValue(result, "qty_mes11");
        objectAccountTreeData.qtyMes12 = UtilSql.getValue(result, "qty_mes12");
        objectAccountTreeData.qtyRef = UtilSql.getValue(result, "qty_ref");
        objectAccountTreeData.qtyOperation = UtilSql.getValue(result, "qty_operation");
        objectAccountTreeData.qtyOperationRef = UtilSql.getValue(result, "qty_operation_ref");
        objectAccountTreeData.qtyOperationMes1 = UtilSql.getValue(result, "qty_operation_mes1");
        objectAccountTreeData.qtyOperationMes2 = UtilSql.getValue(result, "qty_operation_mes2");
        objectAccountTreeData.qtyOperationMes3 = UtilSql.getValue(result, "qty_operation_mes3");
        objectAccountTreeData.qtyOperationMes4 = UtilSql.getValue(result, "qty_operation_mes4");
        objectAccountTreeData.qtyOperationMes5 = UtilSql.getValue(result, "qty_operation_mes5");
        objectAccountTreeData.qtyOperationMes6 = UtilSql.getValue(result, "qty_operation_mes6");
        objectAccountTreeData.qtyOperationMes7 = UtilSql.getValue(result, "qty_operation_mes7");
        objectAccountTreeData.qtyOperationMes8 = UtilSql.getValue(result, "qty_operation_mes8");
        objectAccountTreeData.qtyOperationMes9 = UtilSql.getValue(result, "qty_operation_mes9");
        objectAccountTreeData.qtyOperationMes10 = UtilSql.getValue(result, "qty_operation_mes10");
        objectAccountTreeData.qtyOperationMes11 = UtilSql.getValue(result, "qty_operation_mes11");
        objectAccountTreeData.qtyOperationMes12 = UtilSql.getValue(result, "qty_operation_mes12");
        objectAccountTreeData.qtycredit = UtilSql.getValue(result, "qtycredit");
        objectAccountTreeData.qtycreditRef = UtilSql.getValue(result, "qtycredit_ref");
        objectAccountTreeData.qtycreditMes1 = UtilSql.getValue(result, "qtycredit_mes1");
        objectAccountTreeData.qtycreditMes2 = UtilSql.getValue(result, "qtycredit_mes2");
        objectAccountTreeData.qtycreditMes3 = UtilSql.getValue(result, "qtycredit_mes3");
        objectAccountTreeData.qtycreditMes4 = UtilSql.getValue(result, "qtycredit_mes4");
        objectAccountTreeData.qtycreditMes5 = UtilSql.getValue(result, "qtycredit_mes5");
        objectAccountTreeData.qtycreditMes6 = UtilSql.getValue(result, "qtycredit_mes6");
        objectAccountTreeData.qtycreditMes7 = UtilSql.getValue(result, "qtycredit_mes7");
        objectAccountTreeData.qtycreditMes8 = UtilSql.getValue(result, "qtycredit_mes8");
        objectAccountTreeData.qtycreditMes9 = UtilSql.getValue(result, "qtycredit_mes9");
        objectAccountTreeData.qtycreditMes10 = UtilSql.getValue(result, "qtycredit_mes10");
        objectAccountTreeData.qtycreditMes11 = UtilSql.getValue(result, "qtycredit_mes11");
        objectAccountTreeData.qtycreditMes12 = UtilSql.getValue(result, "qtycredit_mes12");
        objectAccountTreeData.showvaluecond = UtilSql.getValue(result, "showvaluecond");
        objectAccountTreeData.elementlevel = UtilSql.getValue(result, "elementlevel");
        objectAccountTreeData.value = UtilSql.getValue(result, "value");
        objectAccountTreeData.calculated = UtilSql.getValue(result, "calculated");
        objectAccountTreeData.svcreset = UtilSql.getValue(result, "svcreset");
        objectAccountTreeData.svcresetref = UtilSql.getValue(result, "svcresetref");
        objectAccountTreeData.svcresetmes1 = UtilSql.getValue(result, "svcresetmes1");
        objectAccountTreeData.svcresetmes2 = UtilSql.getValue(result, "svcresetmes2");
        objectAccountTreeData.svcresetmes3 = UtilSql.getValue(result, "svcresetmes3");
        objectAccountTreeData.svcresetmes4 = UtilSql.getValue(result, "svcresetmes4");
        objectAccountTreeData.svcresetmes5 = UtilSql.getValue(result, "svcresetmes5");
        objectAccountTreeData.svcresetmes6 = UtilSql.getValue(result, "svcresetmes6");
        objectAccountTreeData.svcresetmes7 = UtilSql.getValue(result, "svcresetmes7");
        objectAccountTreeData.svcresetmes8 = UtilSql.getValue(result, "svcresetmes8");
        objectAccountTreeData.svcresetmes9 = UtilSql.getValue(result, "svcresetmes9");
        objectAccountTreeData.svcresetmes10 = UtilSql.getValue(result, "svcresetmes10");
        objectAccountTreeData.svcresetmes11 = UtilSql.getValue(result, "svcresetmes11");
        objectAccountTreeData.svcresetmes12 = UtilSql.getValue(result, "svcresetmes12");
        objectAccountTreeData.isalwaysshown = UtilSql.getValue(result, "isalwaysshown");
        objectAccountTreeData.sign = UtilSql.getValue(result, "sign");
        objectAccountTreeData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAccountTreeData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AccountTreeData objectAccountTreeData[] = new AccountTreeData[vector.size()];
    vector.copyInto(objectAccountTreeData);
    return(objectAccountTreeData);
  }

  public static AccountTreeData[] selectTrl(ConnectionProvider connectionProvider, String conCodigo, String adLanguage, String adTreeId)    throws ServletException {
    return selectTrl(connectionProvider, conCodigo, adLanguage, adTreeId, 0, 0);
  }

  public static AccountTreeData[] selectTrl(ConnectionProvider connectionProvider, String conCodigo, String adLanguage, String adTreeId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT tn.Node_ID,tn.Parent_ID,tn.SeqNo, m.C_ElementValue_ID AS ID, ((CASE ? WHEN 'Y' THEN TO_CHAR(m.Value) || ' - ' ELSE '' END) || COALESCE(mt.Name, m.Name)) as Name, " +
      "        COALESCE(mt.Description, m.Description) as description ,m.IsSummary, m.ACCOUNTSIGN, " +
      "        m.ShowElement, 0 as element_level, 0 as qty, " +
      "        0 AS qty_mes1, " +
      "        0 AS qty_mes2, " +
      "        0 AS qty_mes3, " +
      "        0 AS qty_mes4, " +
      "        0 AS qty_mes5, " +
      "        0 AS qty_mes6, " +
      "        0 AS qty_mes7, " +
      "        0 AS qty_mes8, " +
      "        0 AS qty_mes9," +
      "        0 AS qty_mes10," +
      "        0 AS qty_mes11, " +
      "        0 AS qty_mes12," +
      "        0 as qty_ref, 0 as qty_operation, 0 as qty_operation_ref," +
      "        0 as qty_operation_mes1," +
      "        0 as qty_operation_mes2," +
      "        0 as qty_operation_mes3," +
      "        0 as qty_operation_mes4, " +
      "        0 as qty_operation_mes5," +
      "        0 as qty_operation_mes6," +
      "        0 as qty_operation_mes7," +
      "        0 as qty_operation_mes8, " +
      "        0 as qty_operation_mes9," +
      "        0 as qty_operation_mes10," +
      "        0 as qty_operation_mes11," +
      "        0 as qty_operation_mes12,  " +
      "        m.ShowValueCond, m.ElementLevel, m.Value, 'N' AS CALCULATED, 'N' AS SVCRESET, 'N' AS SVCRESETREF, " +
      "        'N' AS SVCRESETMES1," +
      "        'N' AS SVCRESETMES2," +
      "        'N' AS SVCRESETMES3," +
      "        'N' AS SVCRESETMES4, " +
      "        'N' AS SVCRESETMES5," +
      "        'N' AS SVCRESETMES6," +
      "        'N' AS SVCRESETMES7," +
      "        'N' AS SVCRESETMES8," +
      "        'N' AS SVCRESETMES9," +
      "        'N' AS SVCRESETMES10," +
      "        'N' AS SVCRESETMES11," +
      "        'N' AS SVCRESETMES12," +
      "        m.isalwaysshown" +
      "        FROM C_ElementValue m left join C_ElementValue_Trl mt on m.C_ElementValue_ID = mt.C_ElementValue_ID " +
      "                                                              and mt.AD_Language = ? ," +
      "              AD_TreeNode tn" +
      "        WHERE tn.IsActive='Y' " +
      "        AND tn.Node_ID = m.C_ElementValue_ID " +
      "        AND tn.AD_Tree_ID = ? " +
      "        ORDER BY COALESCE(tn.Parent_ID, '-1'), tn.SeqNo ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, conCodigo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adTreeId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AccountTreeData objectAccountTreeData = new AccountTreeData();
        objectAccountTreeData.nodeId = UtilSql.getValue(result, "node_id");
        objectAccountTreeData.parentId = UtilSql.getValue(result, "parent_id");
        objectAccountTreeData.seqno = UtilSql.getValue(result, "seqno");
        objectAccountTreeData.id = UtilSql.getValue(result, "id");
        objectAccountTreeData.name = UtilSql.getValue(result, "name");
        objectAccountTreeData.description = UtilSql.getValue(result, "description");
        objectAccountTreeData.issummary = UtilSql.getValue(result, "issummary");
        objectAccountTreeData.accountsign = UtilSql.getValue(result, "accountsign");
        objectAccountTreeData.showelement = UtilSql.getValue(result, "showelement");
        objectAccountTreeData.elementLevel = UtilSql.getValue(result, "element_level");
        objectAccountTreeData.qty = UtilSql.getValue(result, "qty");
        objectAccountTreeData.qtyMes1 = UtilSql.getValue(result, "qty_mes1");
        objectAccountTreeData.qtyMes2 = UtilSql.getValue(result, "qty_mes2");
        objectAccountTreeData.qtyMes3 = UtilSql.getValue(result, "qty_mes3");
        objectAccountTreeData.qtyMes4 = UtilSql.getValue(result, "qty_mes4");
        objectAccountTreeData.qtyMes5 = UtilSql.getValue(result, "qty_mes5");
        objectAccountTreeData.qtyMes6 = UtilSql.getValue(result, "qty_mes6");
        objectAccountTreeData.qtyMes7 = UtilSql.getValue(result, "qty_mes7");
        objectAccountTreeData.qtyMes8 = UtilSql.getValue(result, "qty_mes8");
        objectAccountTreeData.qtyMes9 = UtilSql.getValue(result, "qty_mes9");
        objectAccountTreeData.qtyMes10 = UtilSql.getValue(result, "qty_mes10");
        objectAccountTreeData.qtyMes11 = UtilSql.getValue(result, "qty_mes11");
        objectAccountTreeData.qtyMes12 = UtilSql.getValue(result, "qty_mes12");
        objectAccountTreeData.qtyRef = UtilSql.getValue(result, "qty_ref");
        objectAccountTreeData.qtyOperation = UtilSql.getValue(result, "qty_operation");
        objectAccountTreeData.qtyOperationRef = UtilSql.getValue(result, "qty_operation_ref");
        objectAccountTreeData.qtyOperationMes1 = UtilSql.getValue(result, "qty_operation_mes1");
        objectAccountTreeData.qtyOperationMes2 = UtilSql.getValue(result, "qty_operation_mes2");
        objectAccountTreeData.qtyOperationMes3 = UtilSql.getValue(result, "qty_operation_mes3");
        objectAccountTreeData.qtyOperationMes4 = UtilSql.getValue(result, "qty_operation_mes4");
        objectAccountTreeData.qtyOperationMes5 = UtilSql.getValue(result, "qty_operation_mes5");
        objectAccountTreeData.qtyOperationMes6 = UtilSql.getValue(result, "qty_operation_mes6");
        objectAccountTreeData.qtyOperationMes7 = UtilSql.getValue(result, "qty_operation_mes7");
        objectAccountTreeData.qtyOperationMes8 = UtilSql.getValue(result, "qty_operation_mes8");
        objectAccountTreeData.qtyOperationMes9 = UtilSql.getValue(result, "qty_operation_mes9");
        objectAccountTreeData.qtyOperationMes10 = UtilSql.getValue(result, "qty_operation_mes10");
        objectAccountTreeData.qtyOperationMes11 = UtilSql.getValue(result, "qty_operation_mes11");
        objectAccountTreeData.qtyOperationMes12 = UtilSql.getValue(result, "qty_operation_mes12");
        objectAccountTreeData.showvaluecond = UtilSql.getValue(result, "showvaluecond");
        objectAccountTreeData.elementlevel = UtilSql.getValue(result, "elementlevel");
        objectAccountTreeData.value = UtilSql.getValue(result, "value");
        objectAccountTreeData.calculated = UtilSql.getValue(result, "calculated");
        objectAccountTreeData.svcreset = UtilSql.getValue(result, "svcreset");
        objectAccountTreeData.svcresetref = UtilSql.getValue(result, "svcresetref");
        objectAccountTreeData.svcresetmes1 = UtilSql.getValue(result, "svcresetmes1");
        objectAccountTreeData.svcresetmes2 = UtilSql.getValue(result, "svcresetmes2");
        objectAccountTreeData.svcresetmes3 = UtilSql.getValue(result, "svcresetmes3");
        objectAccountTreeData.svcresetmes4 = UtilSql.getValue(result, "svcresetmes4");
        objectAccountTreeData.svcresetmes5 = UtilSql.getValue(result, "svcresetmes5");
        objectAccountTreeData.svcresetmes6 = UtilSql.getValue(result, "svcresetmes6");
        objectAccountTreeData.svcresetmes7 = UtilSql.getValue(result, "svcresetmes7");
        objectAccountTreeData.svcresetmes8 = UtilSql.getValue(result, "svcresetmes8");
        objectAccountTreeData.svcresetmes9 = UtilSql.getValue(result, "svcresetmes9");
        objectAccountTreeData.svcresetmes10 = UtilSql.getValue(result, "svcresetmes10");
        objectAccountTreeData.svcresetmes11 = UtilSql.getValue(result, "svcresetmes11");
        objectAccountTreeData.svcresetmes12 = UtilSql.getValue(result, "svcresetmes12");
        objectAccountTreeData.isalwaysshown = UtilSql.getValue(result, "isalwaysshown");
        objectAccountTreeData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAccountTreeData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AccountTreeData objectAccountTreeData[] = new AccountTreeData[vector.size()];
    vector.copyInto(objectAccountTreeData);
    return(objectAccountTreeData);
  }

  public static AccountTreeData[] selectAcct(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, String dateFrom, String dateTo, String acctschema, String org, String agno, String dateFromRef, String dateToRef, String agnoRef, String orgM1, String clientM1, String acctschemaM1, String anioRef, String orgM2, String clientM2, String acctschemaM2, String anioRefM2, String orgM3, String clientM3, String acctschemaM3, String anioRefM3, String orgM4, String clientM4, String acctschemaM4, String anioRefM4, String orgM5, String clientM5, String acctschemaM5, String anioRefM5, String orgM6, String clientM6, String acctschemaM6, String anioRefM6, String orgM7, String clientM7, String acctschemaM7, String anioRefM7, String orgM8, String clientM8, String acctschemaM8, String anioRefM8, String orgM9, String clientM9, String acctschemaM9, String anioRefM9, String orgM10, String clientM10, String acctschemaM10, String anioRefM10, String orgM11, String clientM11, String acctschemaM11, String anioRefM11, String orgM12, String clientM12, String acctschemaM12, String anioRefM12)    throws ServletException {
    return selectAcct(connectionProvider, adOrgClient, adUserClient, dateFrom, dateTo, acctschema, org, agno, dateFromRef, dateToRef, agnoRef, orgM1, clientM1, acctschemaM1, anioRef, orgM2, clientM2, acctschemaM2, anioRefM2, orgM3, clientM3, acctschemaM3, anioRefM3, orgM4, clientM4, acctschemaM4, anioRefM4, orgM5, clientM5, acctschemaM5, anioRefM5, orgM6, clientM6, acctschemaM6, anioRefM6, orgM7, clientM7, acctschemaM7, anioRefM7, orgM8, clientM8, acctschemaM8, anioRefM8, orgM9, clientM9, acctschemaM9, anioRefM9, orgM10, clientM10, acctschemaM10, anioRefM10, orgM11, clientM11, acctschemaM11, anioRefM11, orgM12, clientM12, acctschemaM12, anioRefM12, 0, 0);
  }

  public static AccountTreeData[] selectAcct(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, String dateFrom, String dateTo, String acctschema, String org, String agno, String dateFromRef, String dateToRef, String agnoRef, String orgM1, String clientM1, String acctschemaM1, String anioRef, String orgM2, String clientM2, String acctschemaM2, String anioRefM2, String orgM3, String clientM3, String acctschemaM3, String anioRefM3, String orgM4, String clientM4, String acctschemaM4, String anioRefM4, String orgM5, String clientM5, String acctschemaM5, String anioRefM5, String orgM6, String clientM6, String acctschemaM6, String anioRefM6, String orgM7, String clientM7, String acctschemaM7, String anioRefM7, String orgM8, String clientM8, String acctschemaM8, String anioRefM8, String orgM9, String clientM9, String acctschemaM9, String anioRefM9, String orgM10, String clientM10, String acctschemaM10, String anioRefM10, String orgM11, String clientM11, String acctschemaM11, String anioRefM11, String orgM12, String clientM12, String acctschemaM12, String anioRefM12, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT ID, SUM(QTY) AS QTY, SUM(QTYCREDIT) AS QTYCREDIT, SUM(QTY_REF) AS QTY_REF, SUM(QTYCREDIT_REF) AS QTYCREDIT_REF," +
      "        SUM(qty_mes1) AS qty_mes1, " +
      "        SUM(qty_mes2) AS qty_mes2, " +
      "        SUM(qty_mes3) AS qty_mes3, " +
      "        SUM(qty_mes4) AS qty_mes4, " +
      "        SUM(qty_mes5) AS qty_mes5, " +
      "        SUM(qty_mes6) AS qty_mes6, " +
      "        SUM(qty_mes7) AS qty_mes7, " +
      "        SUM(qty_mes8) AS qty_mes8, " +
      "        SUM(qty_mes9) AS qty_mes9," +
      "        SUM(qty_mes10) AS qty_mes10," +
      "        SUM(qty_mes11) AS qty_mes11, " +
      "        SUM(qty_mes12) AS qty_mes12" +
      "        FROM (" +
      "        SELECT m.C_ElementValue_ID as id, (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) AS qty," +
      "        (COALESCE(f.AMTACCTCR,0) - COALESCE(f.AMTACCTDR, 0)) AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "        0 as qty_mes1,  0 as qty_mes2, " +
      "        0 as qty_mes3,  0 as qty_mes4," +
      "        0 as qty_mes5,  0 as qty_mes6, " +
      "        0 as qty_mes7,  0 as qty_mes8, " +
      "        0 as qty_mes9,  0 as qty_mes10, " +
      "        0 as qty_mes11, 0 as qty_mes12" +
      "                FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "                WHERE m.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") " +
      "                AND m.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "                AND 1=1 ";
    strSql = strSql + ((dateFrom==null || dateFrom.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateTo==null || dateTo.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema==null || acctschema.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "                AND f.FACTACCTTYPE <> 'R'" +
      "                AND f.FACTACCTTYPE <> 'C'" +
      "                AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND 0=0 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org==null || org.equals(""))?"":org);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND Y.YEAR IN (";
    strSql = strSql + ((agno==null || agno.equals(""))?"":agno);
    strSql = strSql + 
      ") " +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS QTY, 0 as qtyCredit, (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) AS qty_ref, " +
      "                (COALESCE(f.AMTACCTCR,0) - COALESCE(f.AMTACCTDR, 0)) AS qtyCredit_ref," +
      "                0 as qty_mes1, 0 as qty_mes2," +
      "                0 as qty_mes3, 0 as qty_mes4," +
      "                0 as qty_mes5, 0 as qty_mes6," +
      "                0 as qty_mes7, 0 as qty_mes8, " +
      "                0 as qty_mes9, 0 as qty_mes10, " +
      "                0 as qty_mes11, 0 as qty_mes12" +
      "                FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "                WHERE m.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") " +
      "                AND m.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "                AND 2=2 ";
    strSql = strSql + ((dateFromRef==null || dateFromRef.equals(""))?"":"  AND f.DATEACCT >= to_date(?) ");
    strSql = strSql + ((dateToRef==null || dateToRef.equals(""))?"":"  AND f.DATEACCT < to_date(?) ");
    strSql = strSql + ((acctschema==null || acctschema.equals(""))?"":"  AND f.C_ACCTSCHEMA_ID = ? ");
    strSql = strSql + 
      "                AND f.FACTACCTTYPE <> 'R'" +
      "                AND f.FACTACCTTYPE <> 'C'" +
      "                AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND 1=1 AND f.AD_ORG_ID IN (";
    strSql = strSql + ((org==null || org.equals(""))?"":org);
    strSql = strSql + 
      ")" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND y.YEAR IN (";
    strSql = strSql + ((agnoRef==null || agnoRef.equals(""))?"":agnoRef);
    strSql = strSql + 
      ") " +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_mes1," +
      "                0 as qty_mes2, " +
      "                0 as qty_mes3, 0 as qty_mes4, " +
      "                0 as qty_mes5, 0 as qty_mes6, " +
      "                0 as qty_mes7, 0 as qty_mes8, " +
      "                0 as qty_mes9, 0 as qty_mes10, " +
      "                0 as qty_mes11, 0 as qty_mes12" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 1" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)  " +
      "         UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_mes1," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_mes2," +
      "                0 as qty_mes3, 0 as qty_mes4, " +
      "                0 as qty_mes5, 0 as qty_mes6, " +
      "                0 as qty_mes7, 0 as qty_mes8, " +
      "                0 as qty_mes9, 0 as qty_mes10, " +
      "                0 as qty_mes11, 0 as qty_mes12" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 2" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_mes1," +
      "                0 as qty_mes2," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_mes3," +
      "                 0 as qty_mes4, " +
      "                0 as qty_mes5, 0 as qty_mes6, " +
      "                0 as qty_mes7, 0 as qty_mes8, " +
      "                0 as qty_mes9, 0 as qty_mes10, " +
      "                0 as qty_mes11, 0 as qty_mes12" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 3" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_mes1," +
      "                0 as qty_mes2," +
      "                0 as qty_mes3," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_mes4, " +
      "                0 as qty_mes5, 0 as qty_mes6, " +
      "                0 as qty_mes7, 0 as qty_mes8, " +
      "                0 as qty_mes9, 0 as qty_mes10, " +
      "                0 as qty_mes11, 0 as qty_mes12" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 4" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_mes1," +
      "                0 as qty_mes2," +
      "                0 as qty_mes3," +
      "                0 as qty_mes4," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_mes5, " +
      "                0 as qty_mes6, " +
      "                0 as qty_mes7, 0 as qty_mes8, " +
      "                0 as qty_mes9, 0 as qty_mes10, " +
      "                0 as qty_mes11, 0 as qty_mes12" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 5" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_mes1," +
      "                0 as qty_mes2," +
      "                0 as qty_mes3," +
      "                0 as qty_mes4," +
      "                0 as qty_mes5," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_mes6, " +
      "                0 as qty_mes7, 0 as qty_mes8, " +
      "                0 as qty_mes9, 0 as qty_mes10, " +
      "                0 as qty_mes11, 0 as qty_mes12" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 6" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_mes1," +
      "                0 as qty_mes2," +
      "                0 as qty_mes3," +
      "                0 as qty_mes4," +
      "                0 as qty_mes5," +
      "                0 as qty_mes6," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_mes7, " +
      "                0 as qty_mes8, " +
      "                0 as qty_mes9, 0 as qty_mes10, " +
      "                0 as qty_mes11, 0 as qty_mes12" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 7" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_mes1," +
      "                0 as qty_mes2," +
      "                0 as qty_mes3," +
      "                0 as qty_mes4," +
      "                0 as qty_mes5," +
      "                0 as qty_mes6," +
      "                0 as qty_mes7," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_mes8, " +
      "                0 as qty_mes9, 0 as qty_mes10, " +
      "                0 as qty_mes11, 0 as qty_mes12" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 8" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_mes1," +
      "                0 as qty_mes2," +
      "                0 as qty_mes3," +
      "                0 as qty_mes4," +
      "                0 as qty_mes5," +
      "                0 as qty_mes6," +
      "                0 as qty_mes7," +
      "                0 as qty_mes8," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_mes9, " +
      "                0 as qty_mes10, " +
      "                0 as qty_mes11, 0 as qty_mes12" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 9" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_mes1," +
      "                0 as qty_mes2," +
      "                0 as qty_mes3," +
      "                0 as qty_mes4," +
      "                0 as qty_mes5," +
      "                0 as qty_mes6," +
      "                0 as qty_mes7," +
      "                0 as qty_mes8," +
      "                0 as qty_mes9," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_mes10, " +
      "                0 as qty_mes11, 0 as qty_mes12" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 10" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_mes1," +
      "                0 as qty_mes2," +
      "                0 as qty_mes3," +
      "                0 as qty_mes4," +
      "                0 as qty_mes5," +
      "                0 as qty_mes6," +
      "                0 as qty_mes7," +
      "                0 as qty_mes8," +
      "                0 as qty_mes9," +
      "                0 as qty_mes10," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_mes11, " +
      "                0 as qty_mes12" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 11" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        UNION ALL" +
      "        SELECT m.C_ElementValue_ID as id, 0 AS qty, 0 AS qtyCredit, 0 as qty_ref, 0 as qtyCredit_ref," +
      "                0 as qty_mes1," +
      "                0 as qty_mes2," +
      "                0 as qty_mes3," +
      "                0 as qty_mes4," +
      "                0 as qty_mes5," +
      "                0 as qty_mes6," +
      "                0 as qty_mes7," +
      "                0 as qty_mes8," +
      "                0 as qty_mes9," +
      "                0 as qty_mes10," +
      "                0 as qty_mes11," +
      "                (COALESCE(f.AMTACCTDR,0) - COALESCE(f.AMTACCTCR, 0)) as qty_mes12" +
      "           FROM C_ElementValue m, Fact_Acct f, C_Period p, C_Year y " +
      "          WHERE  p.periodno = 12" +
      "            AND f.DATEACCT >= to_date(p.startdate) " +
      "            AND f.DATEACCT <= to_date(p.enddate) " +
      "            AND f.FACTACCTTYPE <> 'R'" +
      "            AND f.FACTACCTTYPE <> 'C'" +
      "            AND m.C_ElementValue_ID = f.Account_ID" +
      "            AND f.C_PERIOD_ID = p.C_PERIOD_ID " +
      "            AND p.C_YEAR_ID = y.C_YEAR_ID" +
      "            AND (CASE WHEN ? <> '0' THEN  f.AD_ORG_ID IN (?) ELSE f.AD_ORG_ID IN (SELECT AD_ORG_ID FROM AD_ORG WHERE AD_CLIENT_ID = ?) END)" +
      "            AND f.C_ACCTSCHEMA_ID = ?" +
      "            AND y.YEAR IN (?)" +
      "        ) AA" +
      "        GROUP BY ID";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (dateFrom != null && !(dateFrom.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFrom);
      }
      if (dateTo != null && !(dateTo.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTo);
      }
      if (acctschema != null && !(acctschema.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema);
      }
      if (org != null && !(org.equals(""))) {
        }
      if (agno != null && !(agno.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (dateFromRef != null && !(dateFromRef.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateFromRef);
      }
      if (dateToRef != null && !(dateToRef.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateToRef);
      }
      if (acctschema != null && !(acctschema.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschema);
      }
      if (org != null && !(org.equals(""))) {
        }
      if (agnoRef != null && !(agnoRef.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRef);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM3);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM3);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM3);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM3);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM3);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM4);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM4);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM4);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM4);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM4);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM5);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM5);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM5);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM5);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM5);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM6);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM6);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM6);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM6);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM6);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM7);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM7);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM7);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM7);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM7);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM8);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM8);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM8);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM8);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM8);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM9);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM9);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM9);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM9);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM9);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM10);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM10);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM10);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM10);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM10);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM11);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM11);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM11);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM11);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM11);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM12);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, orgM12);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, clientM12);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, acctschemaM12);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anioRefM12);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AccountTreeData objectAccountTreeData = new AccountTreeData();
        objectAccountTreeData.id = UtilSql.getValue(result, "id");
        objectAccountTreeData.qty = UtilSql.getValue(result, "qty");
        objectAccountTreeData.qtycredit = UtilSql.getValue(result, "qtycredit");
        objectAccountTreeData.qtyRef = UtilSql.getValue(result, "qty_ref");
        objectAccountTreeData.qtycreditRef = UtilSql.getValue(result, "qtycredit_ref");
        objectAccountTreeData.qtyMes1 = UtilSql.getValue(result, "qty_mes1");
        objectAccountTreeData.qtyMes2 = UtilSql.getValue(result, "qty_mes2");
        objectAccountTreeData.qtyMes3 = UtilSql.getValue(result, "qty_mes3");
        objectAccountTreeData.qtyMes4 = UtilSql.getValue(result, "qty_mes4");
        objectAccountTreeData.qtyMes5 = UtilSql.getValue(result, "qty_mes5");
        objectAccountTreeData.qtyMes6 = UtilSql.getValue(result, "qty_mes6");
        objectAccountTreeData.qtyMes7 = UtilSql.getValue(result, "qty_mes7");
        objectAccountTreeData.qtyMes8 = UtilSql.getValue(result, "qty_mes8");
        objectAccountTreeData.qtyMes9 = UtilSql.getValue(result, "qty_mes9");
        objectAccountTreeData.qtyMes10 = UtilSql.getValue(result, "qty_mes10");
        objectAccountTreeData.qtyMes11 = UtilSql.getValue(result, "qty_mes11");
        objectAccountTreeData.qtyMes12 = UtilSql.getValue(result, "qty_mes12");
        objectAccountTreeData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAccountTreeData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AccountTreeData objectAccountTreeData[] = new AccountTreeData[vector.size()];
    vector.copyInto(objectAccountTreeData);
    return(objectAccountTreeData);
  }

  public static AccountTreeData[] selectForms(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient)    throws ServletException {
    return selectForms(connectionProvider, adOrgClient, adUserClient, 0, 0);
  }

  public static AccountTreeData[] selectForms(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT m.C_ElementValue_ID as id, o.account_id as node_id, o.sign as ACCOUNTSIGN" +
      "        FROM C_ElementValue m, C_ELEMENTVALUE_OPERAND o  " +
      "        WHERE m.isActive='Y' " +
      "        AND m.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") " +
      "        AND m.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND m.C_ElementValue_ID = o.C_ElementValue_ID" +
      "        AND o.isactive = 'Y' " +
      "        order by m.C_elementvalue_id, o.seqno";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AccountTreeData objectAccountTreeData = new AccountTreeData();
        objectAccountTreeData.id = UtilSql.getValue(result, "id");
        objectAccountTreeData.nodeId = UtilSql.getValue(result, "node_id");
        objectAccountTreeData.accountsign = UtilSql.getValue(result, "accountsign");
        objectAccountTreeData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAccountTreeData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AccountTreeData objectAccountTreeData[] = new AccountTreeData[vector.size()];
    vector.copyInto(objectAccountTreeData);
    return(objectAccountTreeData);
  }

  public static AccountTreeData[] selectOperands(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, String element)    throws ServletException {
    return selectOperands(connectionProvider, adOrgClient, adUserClient, element, 0, 0);
  }

  public static AccountTreeData[] selectOperands(ConnectionProvider connectionProvider, String adOrgClient, String adUserClient, String element, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT m.C_ElementValue_ID as id, o.account_id as node_id, o.sign" +
      "        FROM C_ElementValue m, C_ELEMENTVALUE_OPERAND o, C_ElementValue n" +
      "        WHERE m.isActive='Y' " +
      "        AND m.AD_Org_ID IN(";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") " +
      "        AND m.AD_Client_ID IN(";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND m.C_Element_ID = ?" +
      "        AND m.C_ElementValue_ID = o.C_ElementValue_ID" +
      "        AND n.C_ElementValue_ID = o.C_ElementValue_ID" +
      "        AND o.isactive = 'Y' " +
      "        order by m.C_elementvalue_id, o.seqno";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, element);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        AccountTreeData objectAccountTreeData = new AccountTreeData();
        objectAccountTreeData.id = UtilSql.getValue(result, "id");
        objectAccountTreeData.nodeId = UtilSql.getValue(result, "node_id");
        objectAccountTreeData.sign = UtilSql.getValue(result, "sign");
        objectAccountTreeData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectAccountTreeData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    AccountTreeData objectAccountTreeData[] = new AccountTreeData[vector.size()];
    vector.copyInto(objectAccountTreeData);
    return(objectAccountTreeData);
  }
}
