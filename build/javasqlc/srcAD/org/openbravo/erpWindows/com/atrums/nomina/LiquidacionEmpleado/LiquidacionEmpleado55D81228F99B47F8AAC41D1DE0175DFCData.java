//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.nomina.LiquidacionEmpleado;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class LiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData implements FieldProvider {
static Logger log4j = Logger.getLogger(LiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String cBpartnerId;
  public String cBpartnerIdr;
  public String cDoctypeId;
  public String cDoctypeIdr;
  public String documentno;
  public String tipoLiquidacion;
  public String tipoLiquidacionr;
  public String fechaInicio;
  public String fechaFin;
  public String totalIngreso;
  public String cPeriodId;
  public String cPeriodIdr;
  public String totalEgreso;
  public String totalNeto;
  public String cCurrencyId;
  public String cCurrencyIdr;
  public String docstatus;
  public String docstatusr;
  public String isactive;
  public String processing;
  public String docaccionno;
  public String docaccionnoBtn;
  public String posted;
  public String postedBtn;
  public String processed;
  public String dateacct;
  public String generarpagos;
  public String adClientId;
  public String noLiquidacionEmpleadoId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_idr") || fieldName.equals("cBpartnerIdr"))
      return cBpartnerIdr;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("c_doctype_idr") || fieldName.equals("cDoctypeIdr"))
      return cDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("tipo_liquidacion") || fieldName.equals("tipoLiquidacion"))
      return tipoLiquidacion;
    else if (fieldName.equalsIgnoreCase("tipo_liquidacionr") || fieldName.equals("tipoLiquidacionr"))
      return tipoLiquidacionr;
    else if (fieldName.equalsIgnoreCase("fecha_inicio") || fieldName.equals("fechaInicio"))
      return fechaInicio;
    else if (fieldName.equalsIgnoreCase("fecha_fin") || fieldName.equals("fechaFin"))
      return fechaFin;
    else if (fieldName.equalsIgnoreCase("total_ingreso") || fieldName.equals("totalIngreso"))
      return totalIngreso;
    else if (fieldName.equalsIgnoreCase("c_period_id") || fieldName.equals("cPeriodId"))
      return cPeriodId;
    else if (fieldName.equalsIgnoreCase("c_period_idr") || fieldName.equals("cPeriodIdr"))
      return cPeriodIdr;
    else if (fieldName.equalsIgnoreCase("total_egreso") || fieldName.equals("totalEgreso"))
      return totalEgreso;
    else if (fieldName.equalsIgnoreCase("total_neto") || fieldName.equals("totalNeto"))
      return totalNeto;
    else if (fieldName.equalsIgnoreCase("c_currency_id") || fieldName.equals("cCurrencyId"))
      return cCurrencyId;
    else if (fieldName.equalsIgnoreCase("c_currency_idr") || fieldName.equals("cCurrencyIdr"))
      return cCurrencyIdr;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("docstatusr"))
      return docstatusr;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("processing"))
      return processing;
    else if (fieldName.equalsIgnoreCase("docaccionno"))
      return docaccionno;
    else if (fieldName.equalsIgnoreCase("docaccionno_btn") || fieldName.equals("docaccionnoBtn"))
      return docaccionnoBtn;
    else if (fieldName.equalsIgnoreCase("posted"))
      return posted;
    else if (fieldName.equalsIgnoreCase("posted_btn") || fieldName.equals("postedBtn"))
      return postedBtn;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("dateacct"))
      return dateacct;
    else if (fieldName.equalsIgnoreCase("generarpagos"))
      return generarpagos;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("no_liquidacion_empleado_id") || fieldName.equals("noLiquidacionEmpleadoId"))
      return noLiquidacionEmpleadoId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static LiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static LiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(no_liquidacion_empleado.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_liquidacion_empleado.CreatedBy) as CreatedByR, " +
      "        to_char(no_liquidacion_empleado.Updated, ?) as updated, " +
      "        to_char(no_liquidacion_empleado.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        no_liquidacion_empleado.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_liquidacion_empleado.UpdatedBy) as UpdatedByR," +
      "        no_liquidacion_empleado.AD_Org_ID, " +
      "(CASE WHEN no_liquidacion_empleado.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "no_liquidacion_empleado.C_Bpartner_ID, " +
      "(CASE WHEN no_liquidacion_empleado.C_Bpartner_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'') ) END) AS C_Bpartner_IDR, " +
      "no_liquidacion_empleado.C_Doctype_ID, " +
      "(CASE WHEN no_liquidacion_empleado.C_Doctype_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL3.Name IS NULL THEN TO_CHAR(table3.Name) ELSE TO_CHAR(tableTRL3.Name) END)), ''))),'') ) END) AS C_Doctype_IDR, " +
      "no_liquidacion_empleado.Documentno, " +
      "no_liquidacion_empleado.Tipo_Liquidacion, " +
      "(CASE WHEN no_liquidacion_empleado.Tipo_Liquidacion IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS Tipo_LiquidacionR, " +
      "no_liquidacion_empleado.Fecha_Inicio, " +
      "no_liquidacion_empleado.Fecha_Fin, " +
      "no_liquidacion_empleado.Total_Ingreso, " +
      "no_liquidacion_empleado.C_Period_ID, " +
      "(CASE WHEN no_liquidacion_empleado.C_Period_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table5.Name), ''))),'') ) END) AS C_Period_IDR, " +
      "no_liquidacion_empleado.Total_Egreso, " +
      "no_liquidacion_empleado.Total_Neto, " +
      "no_liquidacion_empleado.C_Currency_ID, " +
      "(CASE WHEN no_liquidacion_empleado.C_Currency_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table6.ISO_Code), ''))),'') ) END) AS C_Currency_IDR, " +
      "no_liquidacion_empleado.Docstatus, " +
      "(CASE WHEN no_liquidacion_empleado.Docstatus IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS DocstatusR, " +
      "COALESCE(no_liquidacion_empleado.Isactive, 'N') AS Isactive, " +
      "no_liquidacion_empleado.Processing, " +
      "no_liquidacion_empleado.Docaccionno, " +
      "list3.name as Docaccionno_BTN, " +
      "no_liquidacion_empleado.Posted, " +
      "list4.name as Posted_BTN, " +
      "COALESCE(no_liquidacion_empleado.Processed, 'N') AS Processed, " +
      "no_liquidacion_empleado.Dateacct, " +
      "no_liquidacion_empleado.Generarpagos, " +
      "no_liquidacion_empleado.AD_Client_ID, " +
      "no_liquidacion_empleado.NO_Liquidacion_Empleado_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM no_liquidacion_empleado left join (select AD_Org_ID, Name from AD_Org) table1 on (no_liquidacion_empleado.AD_Org_ID = table1.AD_Org_ID) left join (select C_BPartner_ID, Name from C_BPartner) table2 on (no_liquidacion_empleado.C_Bpartner_ID = table2.C_BPartner_ID) left join (select C_DocType_ID, Name from C_DocType) table3 on (no_liquidacion_empleado.C_Doctype_ID =  table3.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL3 on (table3.C_DocType_ID = tableTRL3.C_DocType_ID and tableTRL3.AD_Language = ?)  left join ad_ref_list_v list1 on (no_liquidacion_empleado.Tipo_Liquidacion = list1.value and list1.ad_reference_id = 'F73D4D2BFC084F93AD6EC37BE5857EE4' and list1.ad_language = ?)  left join (select C_Period_ID, Name from C_Period) table5 on (no_liquidacion_empleado.C_Period_ID =  table5.C_Period_ID) left join (select C_Currency_ID, ISO_Code from C_Currency) table6 on (no_liquidacion_empleado.C_Currency_ID = table6.C_Currency_ID) left join ad_ref_list_v list2 on (no_liquidacion_empleado.Docstatus = list2.value and list2.ad_reference_id = '31D050E5C2D843B99AD7E9470D9E8579' and list2.ad_language = ?)  left join ad_ref_list_v list3 on (list3.ad_reference_id = '31D050E5C2D843B99AD7E9470D9E8579' and list3.ad_language = ?  AND no_liquidacion_empleado.Docaccionno = TO_CHAR(list3.value)) left join ad_ref_list_v list4 on (list4.ad_reference_id = '234' and list4.ad_language = ?  AND no_liquidacion_empleado.Posted = TO_CHAR(list4.value))" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND no_liquidacion_empleado.NO_Liquidacion_Empleado_ID = ? " +
      "        AND no_liquidacion_empleado.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND no_liquidacion_empleado.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        LiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData = new LiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData();
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.created = UtilSql.getValue(result, "created");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.updated = UtilSql.getValue(result, "updated");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.updatedby = UtilSql.getValue(result, "updatedby");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.cBpartnerIdr = UtilSql.getValue(result, "c_bpartner_idr");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.cDoctypeIdr = UtilSql.getValue(result, "c_doctype_idr");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.documentno = UtilSql.getValue(result, "documentno");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.tipoLiquidacion = UtilSql.getValue(result, "tipo_liquidacion");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.tipoLiquidacionr = UtilSql.getValue(result, "tipo_liquidacionr");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.fechaInicio = UtilSql.getDateValue(result, "fecha_inicio", "dd-MM-yyyy");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.fechaFin = UtilSql.getDateValue(result, "fecha_fin", "dd-MM-yyyy");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.totalIngreso = UtilSql.getValue(result, "total_ingreso");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.cPeriodId = UtilSql.getValue(result, "c_period_id");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.cPeriodIdr = UtilSql.getValue(result, "c_period_idr");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.totalEgreso = UtilSql.getValue(result, "total_egreso");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.totalNeto = UtilSql.getValue(result, "total_neto");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.cCurrencyId = UtilSql.getValue(result, "c_currency_id");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.cCurrencyIdr = UtilSql.getValue(result, "c_currency_idr");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.docstatus = UtilSql.getValue(result, "docstatus");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.docstatusr = UtilSql.getValue(result, "docstatusr");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.isactive = UtilSql.getValue(result, "isactive");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.processing = UtilSql.getValue(result, "processing");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.docaccionno = UtilSql.getValue(result, "docaccionno");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.docaccionnoBtn = UtilSql.getValue(result, "docaccionno_btn");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.posted = UtilSql.getValue(result, "posted");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.postedBtn = UtilSql.getValue(result, "posted_btn");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.processed = UtilSql.getValue(result, "processed");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.dateacct = UtilSql.getDateValue(result, "dateacct", "dd-MM-yyyy");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.generarpagos = UtilSql.getValue(result, "generarpagos");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.noLiquidacionEmpleadoId = UtilSql.getValue(result, "no_liquidacion_empleado_id");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.language = UtilSql.getValue(result, "language");
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.adUserClient = "";
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.adOrgClient = "";
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.createdby = "";
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.trBgcolor = "";
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.totalCount = "";
        objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    LiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[] = new LiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[vector.size()];
    vector.copyInto(objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData);
    return(objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData);
  }

/**
Create a registry
 */
  public static LiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[] set(String totalNeto, String fechaFin, String tipoLiquidacion, String processing, String fechaInicio, String cBpartnerId, String cBpartnerIdr, String cCurrencyId, String dateacct, String processed, String cPeriodId, String adOrgId, String createdby, String createdbyr, String documentno, String totalIngreso, String docstatus, String isactive, String adClientId, String noLiquidacionEmpleadoId, String totalEgreso, String cDoctypeId, String posted, String postedBtn, String updatedby, String updatedbyr, String generarpagos, String docaccionno, String docaccionnoBtn)    throws ServletException {
    LiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[] = new LiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[1];
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0] = new LiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData();
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].created = "";
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].createdbyr = createdbyr;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].updated = "";
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].updatedTimeStamp = "";
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].updatedby = updatedby;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].updatedbyr = updatedbyr;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].adOrgId = adOrgId;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].adOrgIdr = "";
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].cBpartnerId = cBpartnerId;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].cBpartnerIdr = cBpartnerIdr;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].cDoctypeId = cDoctypeId;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].cDoctypeIdr = "";
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].documentno = documentno;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].tipoLiquidacion = tipoLiquidacion;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].tipoLiquidacionr = "";
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].fechaInicio = fechaInicio;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].fechaFin = fechaFin;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].totalIngreso = totalIngreso;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].cPeriodId = cPeriodId;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].cPeriodIdr = "";
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].totalEgreso = totalEgreso;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].totalNeto = totalNeto;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].cCurrencyId = cCurrencyId;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].cCurrencyIdr = "";
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].docstatus = docstatus;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].docstatusr = "";
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].isactive = isactive;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].processing = processing;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].docaccionno = docaccionno;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].docaccionnoBtn = docaccionnoBtn;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].posted = posted;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].postedBtn = postedBtn;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].processed = processed;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].dateacct = dateacct;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].generarpagos = generarpagos;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].adClientId = adClientId;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].noLiquidacionEmpleadoId = noLiquidacionEmpleadoId;
    objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData[0].language = "";
    return objectLiquidacionEmpleado55D81228F99B47F8AAC41D1DE0175DFCData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef4432DA94C9BB42AD9D481AF6D3F9D78F(ConnectionProvider connectionProvider, String C_Bpartner_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT EM_NO_FECHAINGRESO FROM C_BPARTNER  WHERE C_Bpartner_ID= ? ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Bpartner_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "em_no_fechaingreso");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef4D2B622765A2475DA5A5805D45A072A0_0(ConnectionProvider connectionProvider, String C_Bpartner_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as C_Bpartner_ID FROM C_BPartner left join (select C_BPartner_ID, Name from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Bpartner_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_bpartner_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef67C94EE2602642F1BCFF79C61710ED68(ConnectionProvider connectionProvider, String AD_ORG_ID, String AD_CLIENT_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT P.C_PERIOD_ID AS DEFAULTVALUE FROM C_PERIOD P WHERE EXISTS (SELECT * FROM C_PERIODCONTROL PC WHERE P.C_PERIOD_ID=PC.C_PERIOD_ID AND UPPER(PC.PERIODSTATUS)='0') AND EXISTS(SELECT * FROM C_CALENDAR C, C_YEAR Y WHERE Y.C_CALENDAR_ID=C.C_CALENDAR_ID AND P.C_YEAR_ID=Y.C_YEAR_ID AND AD_ISORGINCLUDED(?, C.AD_ORG_ID, ?)<> -1) AND P.AD_CLIENT_ID=? AND NOW() BETWEEN STARTDATE AND ENDDATE ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_ORG_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef8F374859B4324ABCA801C74CA82BE233_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefE8B46E0A7F304F3AB1A67FF3398C007D_2(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE no_liquidacion_empleado" +
      "        SET AD_Org_ID = (?) , C_Bpartner_ID = (?) , C_Doctype_ID = (?) , Documentno = (?) , Tipo_Liquidacion = (?) , Fecha_Inicio = TO_DATE(?) , Fecha_Fin = TO_DATE(?) , Total_Ingreso = TO_NUMBER(?) , C_Period_ID = (?) , Total_Egreso = TO_NUMBER(?) , Total_Neto = TO_NUMBER(?) , C_Currency_ID = (?) , Docstatus = (?) , Isactive = (?) , Processing = (?) , Docaccionno = (?) , Posted = (?) , Processed = (?) , Dateacct = TO_DATE(?) , Generarpagos = (?) , AD_Client_ID = (?) , NO_Liquidacion_Empleado_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE no_liquidacion_empleado.NO_Liquidacion_Empleado_ID = ? " +
      "        AND no_liquidacion_empleado.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_liquidacion_empleado.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoLiquidacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalIngreso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPeriodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalEgreso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalNeto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaccionno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generarpagos);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noLiquidacionEmpleadoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noLiquidacionEmpleadoId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO no_liquidacion_empleado " +
      "        (AD_Org_ID, C_Bpartner_ID, C_Doctype_ID, Documentno, Tipo_Liquidacion, Fecha_Inicio, Fecha_Fin, Total_Ingreso, C_Period_ID, Total_Egreso, Total_Neto, C_Currency_ID, Docstatus, Isactive, Processing, Docaccionno, Posted, Processed, Dateacct, Generarpagos, AD_Client_ID, NO_Liquidacion_Empleado_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), TO_DATE(?), TO_DATE(?), TO_NUMBER(?), (?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoLiquidacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalIngreso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPeriodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalEgreso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalNeto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaccionno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generarpagos);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noLiquidacionEmpleadoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM no_liquidacion_empleado" +
      "        WHERE no_liquidacion_empleado.NO_Liquidacion_Empleado_ID = ? " +
      "        AND no_liquidacion_empleado.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_liquidacion_empleado.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM no_liquidacion_empleado" +
      "         WHERE no_liquidacion_empleado.NO_Liquidacion_Empleado_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM no_liquidacion_empleado" +
      "         WHERE no_liquidacion_empleado.NO_Liquidacion_Empleado_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
