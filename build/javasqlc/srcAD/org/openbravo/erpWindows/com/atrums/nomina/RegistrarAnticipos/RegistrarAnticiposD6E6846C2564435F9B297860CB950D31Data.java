//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.nomina.RegistrarAnticipos;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class RegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data implements FieldProvider {
static Logger log4j = Logger.getLogger(RegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String nombre;
  public String cPeriodId;
  public String cPeriodIdr;
  public String isactive;
  public String processed;
  public String anadirAnticipo;
  public String adClientId;
  public String noRegistraQuincenaId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("nombre"))
      return nombre;
    else if (fieldName.equalsIgnoreCase("c_period_id") || fieldName.equals("cPeriodId"))
      return cPeriodId;
    else if (fieldName.equalsIgnoreCase("c_period_idr") || fieldName.equals("cPeriodIdr"))
      return cPeriodIdr;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("anadir_anticipo") || fieldName.equals("anadirAnticipo"))
      return anadirAnticipo;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("no_registra_quincena_id") || fieldName.equals("noRegistraQuincenaId"))
      return noRegistraQuincenaId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static RegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static RegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(no_registra_quincena.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_registra_quincena.CreatedBy) as CreatedByR, " +
      "        to_char(no_registra_quincena.Updated, ?) as updated, " +
      "        to_char(no_registra_quincena.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        no_registra_quincena.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_registra_quincena.UpdatedBy) as UpdatedByR," +
      "        no_registra_quincena.AD_Org_ID, " +
      "(CASE WHEN no_registra_quincena.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "no_registra_quincena.Nombre, " +
      "no_registra_quincena.C_Period_ID, " +
      "(CASE WHEN no_registra_quincena.C_Period_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'') ) END) AS C_Period_IDR, " +
      "COALESCE(no_registra_quincena.Isactive, 'N') AS Isactive, " +
      "no_registra_quincena.Processed, " +
      "no_registra_quincena.Anadir_Anticipo, " +
      "no_registra_quincena.AD_Client_ID, " +
      "no_registra_quincena.NO_Registra_Quincena_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM no_registra_quincena left join (select AD_Org_ID, Name from AD_Org) table1 on (no_registra_quincena.AD_Org_ID = table1.AD_Org_ID) left join (select C_Period_ID, Name from C_Period) table2 on (no_registra_quincena.C_Period_ID =  table2.C_Period_ID)" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND no_registra_quincena.NO_Registra_Quincena_ID = ? " +
      "        AND no_registra_quincena.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND no_registra_quincena.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        RegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data = new RegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data();
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.created = UtilSql.getValue(result, "created");
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.updated = UtilSql.getValue(result, "updated");
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.nombre = UtilSql.getValue(result, "nombre");
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.cPeriodId = UtilSql.getValue(result, "c_period_id");
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.cPeriodIdr = UtilSql.getValue(result, "c_period_idr");
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.isactive = UtilSql.getValue(result, "isactive");
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.processed = UtilSql.getValue(result, "processed");
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.anadirAnticipo = UtilSql.getValue(result, "anadir_anticipo");
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.noRegistraQuincenaId = UtilSql.getValue(result, "no_registra_quincena_id");
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.language = UtilSql.getValue(result, "language");
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.adUserClient = "";
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.adOrgClient = "";
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.createdby = "";
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.trBgcolor = "";
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.totalCount = "";
        objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    RegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[] = new RegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[vector.size()];
    vector.copyInto(objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data);
    return(objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data);
  }

/**
Create a registry
 */
  public static RegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[] set(String nombre, String createdby, String createdbyr, String noRegistraQuincenaId, String cPeriodId, String processed, String adOrgId, String isactive, String anadirAnticipo, String adClientId, String updatedby, String updatedbyr)    throws ServletException {
    RegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[] = new RegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[1];
    objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[0] = new RegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data();
    objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[0].created = "";
    objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[0].createdbyr = createdbyr;
    objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[0].updated = "";
    objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[0].updatedTimeStamp = "";
    objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[0].updatedby = updatedby;
    objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[0].updatedbyr = updatedbyr;
    objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[0].adOrgId = adOrgId;
    objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[0].adOrgIdr = "";
    objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[0].nombre = nombre;
    objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[0].cPeriodId = cPeriodId;
    objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[0].cPeriodIdr = "";
    objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[0].isactive = isactive;
    objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[0].processed = processed;
    objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[0].anadirAnticipo = anadirAnticipo;
    objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[0].adClientId = adClientId;
    objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[0].noRegistraQuincenaId = noRegistraQuincenaId;
    objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data[0].language = "";
    return objectRegistrarAnticiposD6E6846C2564435F9B297860CB950D31Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef081D59A9AC1D49AF9C42116566EA5AD3_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef55010A206ECE40FAA681D805444CD75B(ConnectionProvider connectionProvider, String AD_ORG_ID, String AD_CLIENT_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT P.C_PERIOD_ID AS DEFAULTVALUE FROM C_PERIOD P WHERE EXISTS (SELECT * FROM C_PERIODCONTROL PC WHERE P.C_PERIOD_ID=PC.C_PERIOD_ID AND UPPER(PC.PERIODSTATUS)='0') AND EXISTS(SELECT * FROM C_CALENDAR C, C_YEAR Y WHERE Y.C_CALENDAR_ID=C.C_CALENDAR_ID AND P.C_YEAR_ID=Y.C_YEAR_ID AND AD_ISORGINCLUDED(?, C.AD_ORG_ID, ?)<> -1) AND P.AD_CLIENT_ID=? AND NOW() BETWEEN STARTDATE AND ENDDATE ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_ORG_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefE2CE8FECF2BA4C1A98F7C937CA94EAB0_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE no_registra_quincena" +
      "        SET AD_Org_ID = (?) , Nombre = (?) , C_Period_ID = (?) , Isactive = (?) , Processed = (?) , Anadir_Anticipo = (?) , AD_Client_ID = (?) , NO_Registra_Quincena_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE no_registra_quincena.NO_Registra_Quincena_ID = ? " +
      "        AND no_registra_quincena.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_registra_quincena.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nombre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPeriodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anadirAnticipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRegistraQuincenaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRegistraQuincenaId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO no_registra_quincena " +
      "        (AD_Org_ID, Nombre, C_Period_ID, Isactive, Processed, Anadir_Anticipo, AD_Client_ID, NO_Registra_Quincena_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nombre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPeriodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anadirAnticipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRegistraQuincenaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM no_registra_quincena" +
      "        WHERE no_registra_quincena.NO_Registra_Quincena_ID = ? " +
      "        AND no_registra_quincena.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_registra_quincena.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM no_registra_quincena" +
      "         WHERE no_registra_quincena.NO_Registra_Quincena_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM no_registra_quincena" +
      "         WHERE no_registra_quincena.NO_Registra_Quincena_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
