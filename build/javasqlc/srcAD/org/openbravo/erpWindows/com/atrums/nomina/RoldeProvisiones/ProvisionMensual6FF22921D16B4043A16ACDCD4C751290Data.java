//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.nomina.RoldeProvisiones;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import java.util.*;

/**
WAD Generated class
 */
class ProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data implements FieldProvider {
static Logger log4j = Logger.getLogger(ProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String valor;
  public String isactive;
  public String processed;
  public String pago;
  public String cDoctypeId;
  public String cDoctypeIdr;
  public String documentno;
  public String docstatus;
  public String docstatusr;
  public String docaccionno;
  public String docaccionnoBtn;
  public String dateacct;
  public String processing;
  public String posted;
  public String postedBtn;
  public String cPeriodId;
  public String cPeriodIdr;
  public String adOrgId;
  public String noRolProvisionLineMesId;
  public String adClientId;
  public String noRolPagoProvisionLineId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("valor"))
      return valor;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("pago"))
      return pago;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("c_doctype_idr") || fieldName.equals("cDoctypeIdr"))
      return cDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("docstatusr"))
      return docstatusr;
    else if (fieldName.equalsIgnoreCase("docaccionno"))
      return docaccionno;
    else if (fieldName.equalsIgnoreCase("docaccionno_btn") || fieldName.equals("docaccionnoBtn"))
      return docaccionnoBtn;
    else if (fieldName.equalsIgnoreCase("dateacct"))
      return dateacct;
    else if (fieldName.equalsIgnoreCase("processing"))
      return processing;
    else if (fieldName.equalsIgnoreCase("posted"))
      return posted;
    else if (fieldName.equalsIgnoreCase("posted_btn") || fieldName.equals("postedBtn"))
      return postedBtn;
    else if (fieldName.equalsIgnoreCase("c_period_id") || fieldName.equals("cPeriodId"))
      return cPeriodId;
    else if (fieldName.equalsIgnoreCase("c_period_idr") || fieldName.equals("cPeriodIdr"))
      return cPeriodIdr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("no_rol_provision_line_mes_id") || fieldName.equals("noRolProvisionLineMesId"))
      return noRolProvisionLineMesId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("no_rol_pago_provision_line_id") || fieldName.equals("noRolPagoProvisionLineId"))
      return noRolPagoProvisionLineId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static ProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String noRolPagoProvisionLineId, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, noRolPagoProvisionLineId, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static ProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String noRolPagoProvisionLineId, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(no_rol_provision_line_mes.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_rol_provision_line_mes.CreatedBy) as CreatedByR, " +
      "        to_char(no_rol_provision_line_mes.Updated, ?) as updated, " +
      "        to_char(no_rol_provision_line_mes.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        no_rol_provision_line_mes.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_rol_provision_line_mes.UpdatedBy) as UpdatedByR," +
      "        no_rol_provision_line_mes.Valor, " +
      "COALESCE(no_rol_provision_line_mes.Isactive, 'N') AS Isactive, " +
      "COALESCE(no_rol_provision_line_mes.Processed, 'N') AS Processed, " +
      "COALESCE(no_rol_provision_line_mes.Pago, 'N') AS Pago, " +
      "no_rol_provision_line_mes.C_Doctype_ID, " +
      "(CASE WHEN no_rol_provision_line_mes.C_Doctype_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL1.Name IS NULL THEN TO_CHAR(table1.Name) ELSE TO_CHAR(tableTRL1.Name) END)), ''))),'') ) END) AS C_Doctype_IDR, " +
      "no_rol_provision_line_mes.Documentno, " +
      "no_rol_provision_line_mes.Docstatus, " +
      "(CASE WHEN no_rol_provision_line_mes.Docstatus IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS DocstatusR, " +
      "no_rol_provision_line_mes.Docaccionno, " +
      "list2.name as Docaccionno_BTN, " +
      "no_rol_provision_line_mes.Dateacct, " +
      "no_rol_provision_line_mes.Processing, " +
      "no_rol_provision_line_mes.Posted, " +
      "list3.name as Posted_BTN, " +
      "no_rol_provision_line_mes.C_Period_ID, " +
      "(CASE WHEN no_rol_provision_line_mes.C_Period_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.Name), ''))),'') ) END) AS C_Period_IDR, " +
      "no_rol_provision_line_mes.AD_Org_ID, " +
      "no_rol_provision_line_mes.NO_Rol_Provision_Line_Mes_ID, " +
      "no_rol_provision_line_mes.AD_Client_ID, " +
      "no_rol_provision_line_mes.NO_Rol_Pago_Provision_Line_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM no_rol_provision_line_mes left join (select C_DocType_ID, Name from C_DocType) table1 on (no_rol_provision_line_mes.C_Doctype_ID =  table1.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL1 on (table1.C_DocType_ID = tableTRL1.C_DocType_ID and tableTRL1.AD_Language = ?)  left join ad_ref_list_v list1 on (no_rol_provision_line_mes.Docstatus = list1.value and list1.ad_reference_id = '31D050E5C2D843B99AD7E9470D9E8579' and list1.ad_language = ?)  left join ad_ref_list_v list2 on (list2.ad_reference_id = '31D050E5C2D843B99AD7E9470D9E8579' and list2.ad_language = ?  AND no_rol_provision_line_mes.Docaccionno = TO_CHAR(list2.value)) left join ad_ref_list_v list3 on (list3.ad_reference_id = '234' and list3.ad_language = ?  AND no_rol_provision_line_mes.Posted = TO_CHAR(list3.value)) left join (select C_Period_ID, Name from C_Period) table3 on (no_rol_provision_line_mes.C_Period_ID =  table3.C_Period_ID)" +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((noRolPagoProvisionLineId==null || noRolPagoProvisionLineId.equals(""))?"":"  AND no_rol_provision_line_mes.NO_Rol_Pago_Provision_Line_ID = ?  ");
    strSql = strSql + 
      "        AND no_rol_provision_line_mes.NO_Rol_Provision_Line_Mes_ID = ? " +
      "        AND no_rol_provision_line_mes.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND no_rol_provision_line_mes.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (noRolPagoProvisionLineId != null && !(noRolPagoProvisionLineId.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionLineId);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data = new ProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data();
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.created = UtilSql.getValue(result, "created");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.updated = UtilSql.getValue(result, "updated");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.valor = UtilSql.getValue(result, "valor");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.isactive = UtilSql.getValue(result, "isactive");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.processed = UtilSql.getValue(result, "processed");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.pago = UtilSql.getValue(result, "pago");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.cDoctypeIdr = UtilSql.getValue(result, "c_doctype_idr");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.documentno = UtilSql.getValue(result, "documentno");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.docstatus = UtilSql.getValue(result, "docstatus");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.docstatusr = UtilSql.getValue(result, "docstatusr");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.docaccionno = UtilSql.getValue(result, "docaccionno");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.docaccionnoBtn = UtilSql.getValue(result, "docaccionno_btn");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.dateacct = UtilSql.getDateValue(result, "dateacct", "dd-MM-yyyy");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.processing = UtilSql.getValue(result, "processing");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.posted = UtilSql.getValue(result, "posted");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.postedBtn = UtilSql.getValue(result, "posted_btn");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.cPeriodId = UtilSql.getValue(result, "c_period_id");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.cPeriodIdr = UtilSql.getValue(result, "c_period_idr");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.noRolProvisionLineMesId = UtilSql.getValue(result, "no_rol_provision_line_mes_id");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.noRolPagoProvisionLineId = UtilSql.getValue(result, "no_rol_pago_provision_line_id");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.language = UtilSql.getValue(result, "language");
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.adUserClient = "";
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.adOrgClient = "";
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.createdby = "";
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.trBgcolor = "";
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.totalCount = "";
        objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[] = new ProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[vector.size()];
    vector.copyInto(objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data);
    return(objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data);
  }

/**
Create a registry
 */
  public static ProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[] set(String noRolPagoProvisionLineId, String docstatus, String docaccionno, String docaccionnoBtn, String adOrgId, String valor, String dateacct, String posted, String postedBtn, String documentno, String createdby, String createdbyr, String updatedby, String updatedbyr, String isactive, String processing, String cPeriodId, String pago, String adClientId, String processed, String noRolProvisionLineMesId, String cDoctypeId)    throws ServletException {
    ProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[] = new ProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[1];
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0] = new ProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data();
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].created = "";
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].createdbyr = createdbyr;
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].updated = "";
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].updatedTimeStamp = "";
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].updatedby = updatedby;
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].updatedbyr = updatedbyr;
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].valor = valor;
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].isactive = isactive;
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].processed = processed;
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].pago = pago;
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].cDoctypeId = cDoctypeId;
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].cDoctypeIdr = "";
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].documentno = documentno;
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].docstatus = docstatus;
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].docstatusr = "";
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].docaccionno = docaccionno;
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].docaccionnoBtn = docaccionnoBtn;
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].dateacct = dateacct;
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].processing = processing;
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].posted = posted;
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].postedBtn = postedBtn;
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].cPeriodId = cPeriodId;
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].cPeriodIdr = "";
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].adOrgId = adOrgId;
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].noRolProvisionLineMesId = noRolProvisionLineMesId;
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].adClientId = adClientId;
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].noRolPagoProvisionLineId = noRolPagoProvisionLineId;
    objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data[0].language = "";
    return objectProvisionMensual6FF22921D16B4043A16ACDCD4C751290Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef5BB1F33113644BD8A7AB5D6E0602BEA7_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef5F8F9A93A9464997BCE42F6C3ACF2319_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef8E284351C635427393E35181D5A89179(ConnectionProvider connectionProvider, String AD_ORG_ID, String AD_CLIENT_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT P.C_PERIOD_ID AS DEFAULTVALUE FROM C_PERIOD P WHERE EXISTS (SELECT * FROM C_PERIODCONTROL PC WHERE P.C_PERIOD_ID=PC.C_PERIOD_ID AND UPPER(PC.PERIODSTATUS)='0') AND EXISTS(SELECT * FROM C_CALENDAR C, C_YEAR Y WHERE Y.C_CALENDAR_ID=C.C_CALENDAR_ID AND P.C_YEAR_ID=Y.C_YEAR_ID AND AD_ISORGINCLUDED(?, C.AD_ORG_ID, ?)<> -1) AND P.AD_CLIENT_ID=? AND NOW() BETWEEN STARTDATE AND ENDDATE ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_ORG_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
return the parent ID
 */
  public static String selectParentID(ConnectionProvider connectionProvider, String key)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT no_rol_provision_line_mes.NO_Rol_Pago_Provision_Line_ID AS NAME" +
      "        FROM no_rol_provision_line_mes" +
      "        WHERE no_rol_provision_line_mes.NO_Rol_Provision_Line_Mes_ID = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParent(ConnectionProvider connectionProvider, String noRolPagoProvisionLineId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.Line), '')) || ' - ' || TO_CHAR(COALESCE(TO_CHAR(table1.Valor), ''))) AS NAME FROM no_rol_pago_provision_line left join (select NO_Rol_Pago_Provision_Line_ID, Line, Valor from NO_Rol_Pago_Provision_Line) table1 on (no_rol_pago_provision_line.NO_Rol_Pago_Provision_Line_ID = table1.NO_Rol_Pago_Provision_Line_ID) WHERE no_rol_pago_provision_line.NO_Rol_Pago_Provision_Line_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionLineId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParentTrl(ConnectionProvider connectionProvider, String noRolPagoProvisionLineId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.Line), '')) || ' - ' || TO_CHAR(COALESCE(TO_CHAR(table1.Valor), ''))) AS NAME FROM no_rol_pago_provision_line left join (select NO_Rol_Pago_Provision_Line_ID, Line, Valor from NO_Rol_Pago_Provision_Line) table1 on (no_rol_pago_provision_line.NO_Rol_Pago_Provision_Line_ID = table1.NO_Rol_Pago_Provision_Line_ID) WHERE no_rol_pago_provision_line.NO_Rol_Pago_Provision_Line_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionLineId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE no_rol_provision_line_mes" +
      "        SET Valor = TO_NUMBER(?) , Isactive = (?) , Processed = (?) , Pago = (?) , C_Doctype_ID = (?) , Documentno = (?) , Docstatus = (?) , Docaccionno = (?) , Dateacct = TO_DATE(?) , Processing = (?) , Posted = (?) , C_Period_ID = (?) , AD_Org_ID = (?) , NO_Rol_Provision_Line_Mes_ID = (?) , AD_Client_ID = (?) , NO_Rol_Pago_Provision_Line_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE no_rol_provision_line_mes.NO_Rol_Provision_Line_Mes_ID = ? " +
      "                 AND no_rol_provision_line_mes.NO_Rol_Pago_Provision_Line_ID = ? " +
      "        AND no_rol_provision_line_mes.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_rol_provision_line_mes.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valor);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaccionno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPeriodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolProvisionLineMesId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionLineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolProvisionLineMesId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionLineId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO no_rol_provision_line_mes " +
      "        (Valor, Isactive, Processed, Pago, C_Doctype_ID, Documentno, Docstatus, Docaccionno, Dateacct, Processing, Posted, C_Period_ID, AD_Org_ID, NO_Rol_Provision_Line_Mes_ID, AD_Client_ID, NO_Rol_Pago_Provision_Line_ID, created, createdby, updated, updatedBy)" +
      "        VALUES (TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valor);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaccionno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPeriodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolProvisionLineMesId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionLineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String noRolPagoProvisionLineId, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM no_rol_provision_line_mes" +
      "        WHERE no_rol_provision_line_mes.NO_Rol_Provision_Line_Mes_ID = ? " +
      "                 AND no_rol_provision_line_mes.NO_Rol_Pago_Provision_Line_ID = ? " +
      "        AND no_rol_provision_line_mes.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_rol_provision_line_mes.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionLineId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM no_rol_provision_line_mes" +
      "         WHERE no_rol_provision_line_mes.NO_Rol_Provision_Line_Mes_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM no_rol_provision_line_mes" +
      "         WHERE no_rol_provision_line_mes.NO_Rol_Provision_Line_Mes_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
