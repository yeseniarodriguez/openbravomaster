package com.atrums.offline.feletronica.process;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.OrganizationInformation;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.shipping.ShippingCompany;

public class ATECOFF_GenerarGuiaDespachoXML {

  public static boolean generarFacturaXMLGre(ShipmentInOut spiDato, ConnectionProvider conn,
      String strUser, boolean enviarSRI, OBError msg) throws Exception {
    File flXml = null;
    String fileString = null;
    Document docXML = null;
    Client cltDato = null;
    OutputFormat ofFormat = null;
    DocumentType dctDato = null;
    OrganizationInformation oriDato = null;
    BusinessPartner bspDato = null;
    BusinessPartner bspDatoDest = null;
    ShippingCompany shcDato = null;
    Organization orgDato = null;
    ATECOFFGenerarXmlData[] axmlDirMatriz = null;
    ATECOFFGenerarXmlData[] axmlDirec = null;
    ATECOFFGenerarXmlData[] strDirPartida = null;
    ATECOFFGenerarXmlData[] transp = null;
    ATECOFFGenerarXmlData[] strDirecDestino = null;
    ATECOFFGenerarXmlData[] axmlDocSustento = null;
    ATECOFFGenerarXmlData[] axmlDetalles = null;
    ATECOFFGenerarXmlData[] axmlEmail = null;

    String emailSoporte = "soporte@atrums.com";

    Hashtable<String, String> hstClaveAcceso = new Hashtable<String, String>();

    try {

      flXml = File.createTempFile("documento.xml", null);
      flXml.deleteOnExit();

      docXML = DocumentHelper.createDocument();

      ofFormat = OutputFormat.createPrettyPrint();
      ofFormat.setEncoding("utf-8");
      ofFormat.setTrimText(true);

      final Element elmgre = docXML.addElement("guiaRemision");
      elmgre.addAttribute("id", "comprobante");
      elmgre.addAttribute("version", "1.0.0");

      final Element elminftri = elmgre.addElement("infoTributaria");

      if (spiDato.getClient() != null) {
        cltDato = OBDal.getInstance().get(Client.class, spiDato.getClient().getId());

        Date cldFechaIn = spiDato.getMovementDate();
        SimpleDateFormat sdfFormato = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdfFormatoClave = new SimpleDateFormat("ddMMyyyy");

        dctDato = OBDal.getInstance().get(DocumentType.class, spiDato.getDocumentType().getId());

        oriDato = OBDal.getInstance().get(OrganizationInformation.class,
            spiDato.getOrganization().getId());

        if (spiDato.getShippingCompany() == null) {
          bspDato = OBDal.getInstance().get(BusinessPartner.class,
              spiDato.getBusinessPartner().getId());
        } else {
          shcDato = OBDal.getInstance().get(ShippingCompany.class,
              spiDato.getShippingCompany().getId());

          if (shcDato.getBusinessPartner() != null) {
            bspDato = OBDal.getInstance().get(BusinessPartner.class,
                shcDato.getBusinessPartner().getId());
          } else {
            msg.setType("Error");
            msg.setMessage(
                "Es necesario un tercero en el transportista de la guía de despacho nro: "
                    + spiDato.getDocumentNo());
            msg.setTitle("@Error@");
            return false;
          }
        }

        bspDatoDest = OBDal.getInstance().get(BusinessPartner.class,
            spiDato.getBusinessPartner().getId());

        axmlDirMatriz = ATECOFFGenerarXmlData.methodSelDirMatriz(conn, spiDato.getClient().getId());

        String strDirMat = "";
        if (axmlDirMatriz != null && axmlDirMatriz.length == 1) {
          strDirMat = axmlDirMatriz[0].dato1;
        }

        axmlDirec = ATECOFFGenerarXmlData.methodSeleccionarDirec(conn,
            spiDato.getOrganization().getId());

        String strDir = "";
        if (axmlDirec != null && axmlDirec.length == 1) {
          strDir = axmlDirec[0].dato1;
        }

        orgDato = OBDal.getInstance().get(Organization.class, spiDato.getOrganization().getId());

        if (ATECOFF_Operacion_Auxiliares.generarCabecera(elminftri, cltDato.getAtecfeTipoambiente(),
            dctDato.getCoTipoComprobanteAutorizadorSRI().toString(), oriDato.getTaxID(),
            orgDato.getCoNroEstab(), orgDato.getCoPuntoEmision(), spiDato.getDocumentNo(),
            cltDato.getAtecfeCodinumerico(), cltDato.getAtecfeTipoemisi(),
            sdfFormatoClave.format(cldFechaIn), strDirMat, cltDato.getName(), cltDato.getName(),
            msg, hstClaveAcceso) && dctDato != null && oriDato != null && bspDato != null
            && bspDatoDest != null) {

          final Element elmcomgre = elmgre.addElement("infoGuiaRemision");

          if (!strDir.equals("")) {
            elmcomgre.addElement("dirEstablecimiento")
                .addText(ATECOFF_Operacion_Auxiliares.normalizacionPalabras(strDir));
          } else {
            msg.setType("Error");
            msg.setMessage(
                "Es necesaria la dirección del establecimiento de la guía de despacho nro: "
                    + spiDato.getDocumentNo());
            msg.setTitle("@Error@");
            return false;
          }

          if (!strDir.equals("")) {
            strDirPartida = ATECOFFGenerarXmlData.methodSeleccionarDirecBodega(conn,
                spiDato.getWarehouse().getId());

            if (strDirPartida != null) {
              if (strDirPartida.length > 0) {
                elmcomgre.addElement("dirPartida").addText(
                    ATECOFF_Operacion_Auxiliares.normalizacionPalabras(strDirPartida[0].dato1));
              } else {
                msg.setType("Error");
                msg.setMessage(
                    "Es necesaria la dirección de la bodega de partida de la guía de despacho nro: "
                        + spiDato.getDocumentNo());
                msg.setTitle("@Error@");
                return false;
              }
            } else {
              msg.setType("Error");
              msg.setMessage(
                  "Es necesaria la dirección de la bodega de partida de la guía de despacho nro: "
                      + spiDato.getDocumentNo());
              msg.setTitle("@Error@");
              return false;
            }
          }

          if (bspDato.getName() != null) {
            elmcomgre.addElement("razonSocialTransportista").addText(bspDato.getName());
          } else if (bspDato.getName2() != null) {
            elmcomgre.addElement("razonSocialTransportista").addText(bspDato.getName2());
          } else {
            msg.setType("Error");
            msg.setMessage("Es necesaria la razón social del comprador de la guía de despacho nro: "
                + spiDato.getDocumentNo());
            msg.setTitle("@Error@");
            return false;
          }

          if (bspDato.getCOTipoIdentificacion().toString().equals("01")
              || bspDato.getCOTipoIdentificacion().toString().equals("1")) {
            elmcomgre.addElement("tipoIdentificacionTransportista").addText("04");
          } else if (bspDato.getCOTipoIdentificacion().toString().equals("02")
              || bspDato.getCOTipoIdentificacion().toString().equals("2")) {
            elmcomgre.addElement("tipoIdentificacionTransportista").addText("05");
          } else if (bspDato.getCOTipoIdentificacion().toString().equals("03")
              || bspDato.getCOTipoIdentificacion().toString().equals("3")) {
            elmcomgre.addElement("tipoIdentificacionTransportista").addText("06");
          } else if (bspDato.getCOTipoIdentificacion().toString().equals("07")
              || bspDato.getCOTipoIdentificacion().toString().equals("7")) {
            elmcomgre.addElement("tipoIdentificacionTransportista").addText("07");
          } else {
            msg.setType("Error");
            msg.setMessage("El transportista debe tener un tipo de identificacion válido");
            msg.setTitle("@Error@");
            return false;
          }

          if (bspDato.getTaxID() != null) {
            elmcomgre.addElement("rucTransportista").addText(bspDato.getTaxID());
          } else {
            msg.setType("Error");
            msg.setMessage("El transportista debe tener el ruc válido");
            msg.setTitle("@Error@");
            return false;
          }

          if (cltDato.isAtecfeObligcontabi()) {
            elmcomgre.addElement("obligadoContabilidad").addText("SI");
          } else {
            elmcomgre.addElement("obligadoContabilidad").addText("NO");
          }

          elmcomgre.addElement("fechaIniTransporte")
              .addText(sdfFormato.format(spiDato.getMovementDate()));

          elmcomgre.addElement("fechaFinTransporte")
              .addText(sdfFormato.format(spiDato.getMovementDate()));

          if (shcDato != null) {
            if (shcDato.getDescription() != null) {
              elmcomgre.addElement("placa").addText(
                  ATECOFF_Operacion_Auxiliares.normalizacionPalabras(shcDato.getDescription()));
            } else {
              msg.setType("Error");
              msg.setMessage("Es necesaria la placa del transportista de la guía de despacho nro: "
                  + spiDato.getDocumentNo());
              msg.setTitle("@Error@");
              return false;
            }
          } else {
            transp = ATECOFFGenerarXmlData.methodSelectTransport(conn);

            if (transp.length > 0) {
              elmcomgre.addElement("placa")
                  .addText(ATECOFF_Operacion_Auxiliares.normalizacionPalabras(transp[0].dato1));
            } else {
              msg.setType("Error");
              msg.setMessage("Es necesaria la placa del transportista de la guía de despacho nro: "
                  + spiDato.getDocumentNo());
              msg.setTitle("@Error@");
              return false;
            }
          }

          final Element elmdestina = elmgre.addElement("destinatarios");
          final Element elmdestin = elmdestina.addElement("destinatario");

          elmdestin.addElement("identificacionDestinatario").addText(bspDatoDest.getTaxID());

          if (bspDatoDest.getName() != null) {
            elmdestin.addElement("razonSocialDestinatario")
                .addText(ATECOFF_Operacion_Auxiliares.normalizacionPalabras(bspDatoDest.getName()));
          } else if (bspDatoDest.getName2() != null) {
            elmdestin.addElement("razonSocialDestinatario").addText(
                ATECOFF_Operacion_Auxiliares.normalizacionPalabras(bspDatoDest.getName2()));
          } else {
            msg.setType("Error");
            msg.setMessage(
                "Es necesaria la razón social del destinatario de la guía de despacho nro: "
                    + spiDato.getDocumentNo());
            msg.setTitle("@Error@");
            return false;
          }

          strDirecDestino = ATECOFFGenerarXmlData.methodSeleccionarDirecDestinatario(conn,
              bspDatoDest.getId());

          if (strDirecDestino != null) {
            if (strDirecDestino.length > 0) {
              elmdestin.addElement("dirDestinatario").addText(strDirecDestino[0].dato1);
            } else {
              msg.setType("Error");
              msg.setMessage(
                  "Es necesaria la dirección del destinatario de la guía de despacho nro: "
                      + spiDato.getDocumentNo());
              msg.setTitle("@Error@");
              return false;
            }
          }

          if (spiDato.getDescription() != null) {
            elmdestin.addElement("motivoTraslado").addText(spiDato.getDescription());
          } else {
            elmdestin.addElement("motivoTraslado").addText("Transporte".toUpperCase());
          }

          elmdestin.addElement("codDocSustento").addText("01");

          axmlDocSustento = ATECOFFGenerarXmlData.methodSeleccionarDatoSusteno(conn,
              spiDato.getId());

          if (axmlDocSustento.length > 0) {
            elmdestin.addElement("numDocSustento").addText(axmlDocSustento[0].dato1);
            elmdestin.addElement("fechaEmisionDocSustento").addText(axmlDocSustento[0].dato2);
          }

          axmlDetalles = ATECOFFGenerarXmlData.methodSeleccionarDetallesGuia(conn, spiDato.getId());

          if (axmlDetalles != null && axmlDetalles.length > 0) {
            Element elmdetalles = elmdestin.addElement("detalles");
            for (int i = 0; i < axmlDetalles.length; i++) {
              Element elmdetalle = elmdetalles.addElement("detalle");
              elmdetalle.addElement("codigoInterno").addText(
                  ATECOFF_Operacion_Auxiliares.normalizacionPalabras(axmlDetalles[i].dato1));
              elmdetalle.addElement("codigoAdicional").addText(
                  ATECOFF_Operacion_Auxiliares.normalizacionPalabras(axmlDetalles[i].dato1));
              elmdetalle.addElement("descripcion").addText(
                  ATECOFF_Operacion_Auxiliares.normalizacionPalabras(axmlDetalles[i].dato3));
              elmdetalle.addElement("cantidad").addText(axmlDetalles[i].dato4);
            }
          }

          ATECOFF_Operacion_Auxiliares.addCamposAdic(elmgre, conn, spiDato.getClient().getId());

          final XMLWriter writer = new XMLWriter(
              new OutputStreamWriter(new FileOutputStream(flXml), "utf-8"), ofFormat);
          writer.write(docXML);
          writer.flush();
          writer.close();

          flXml = ATECOFF_Operacion_Auxiliares.firmarDocumento(flXml, conn, strUser,
              spiDato.getClient().getId(), msg);

          if (flXml != null) {

            if (enviarSRI) {
              Calendar cldFecha = new GregorianCalendar();
              String strFechaAut = "";

              strFechaAut = sdfFormato.format(cldFecha.getTime());

              byte[] bytes = ATECOFF_Operacion_Auxiliares.filetobyte(flXml);
              fileString = new String(bytes, "UTF-8");

              String mensaje = "";

              ATECOFF_SRIDocumentoAutorizado autorizadoPre = new ATECOFF_SRIDocumentoAutorizado();
              ATECOFF_SRIDocumentoRecibido recibido = new ATECOFF_SRIDocumentoRecibido();

              ATECOFF_ServiceAutorizacion serviceAutorizacionPre = new ATECOFF_ServiceAutorizacion(
                  cltDato.getAtecfeTipoambiente(), hstClaveAcceso.get("claveacc"));

              autorizadoPre = serviceAutorizacionPre.CallAutorizado();

              if (!autorizadoPre.getEstadoespecifico().equals("AUT")) {
                ATECOFF_ServiceRecibido serviceRecibido = new ATECOFF_ServiceRecibido(
                    cltDato.getAtecfeTipoambiente(), fileString);
                recibido = serviceRecibido.CallRecibido();
              } else {
                recibido.setEstado("RECIBIDO");
                recibido.setEstadoespecifico("REC");
              }

              if (recibido.getInformacion() != null) {
                mensaje = recibido.getMensaje() + " - "
                    + recibido.getInformacion().replace("'", "");
              }

              if (recibido.getEstadoespecifico().equals("REC")) {
                ATECOFF_SRIDocumentoAutorizado autorizado = new ATECOFF_SRIDocumentoAutorizado();
                ATECOFF_ServiceAutorizacion serviceAutorizacion = new ATECOFF_ServiceAutorizacion(
                    cltDato.getAtecfeTipoambiente(), hstClaveAcceso.get("claveacc"));

                // hstClaveAcceso.get("claveacc")
                autorizado = serviceAutorizacion.CallAutorizado();

                if (autorizado.getEstadoespecifico().equals("AUT")) {
                  if (autorizado.getInformacion() != null) {
                    mensaje = autorizado.getMensaje() + " - "
                        + autorizado.getInformacion().replace("'", "");
                  }

                  List<File> lisdoc = new ArrayList<File>();
                  lisdoc.add(autorizado.getDocFile());

                  File flPdf = null;

                  flPdf = ATECOFF_Operacion_Auxiliares.generarPDF(conn,
                      "@basedesign@/com/atrums/felectronica/erpReport/Rpt_Guia.jrxml", "Guia",
                      spiDato.getId());

                  if (flPdf != null)
                    lisdoc.add(flPdf);

                  axmlEmail = ATECOFFGenerarXmlData.methodSeleccionarEmail(conn,
                      spiDato.getBusinessPartner().getId());

                  String strSubject = "Factura Electronica de " + axmlEmail[0].dato2;

                  String strContenido = "Señor/a\n" + axmlEmail[0].dato3
                      + "\n\nUd tiene un documento electronico que puede ser consultada en: "
                      + axmlEmail[0].dato4 + "\nCon los credeciales: \n\n -Usuario: "
                      + axmlEmail[0].dato5 + "\n -Contraseña: " + axmlEmail[0].dato5
                      + "\n\n\nAtentamente " + axmlEmail[0].dato2;

                  String strMensaje = "Su documento a sido autorizado por el SRI, ";

                  if (ATECOFF_Operacion_Auxiliares.enviarCorreo(axmlEmail[0].dato1, strSubject,
                      strContenido, null, lisdoc, false)) {
                    msg.setType("Success");
                    msg.setTitle("Mensaje");
                    msg.setMessage(strMensaje + "y fue enviado al correo electronico del cliente");
                    flXml.delete();

                    mensaje = mensaje.equals("") ? "Se envio el email cliente, AUTORIZADO"
                        : ", Se envio el email al cliente, AUTORIZADO";

                    ATECOFFGenerarXmlData.methodActualizarMinoutOffline(conn,
                        autorizado.getDocXML(), hstClaveAcceso.get("claveacc"), mensaje, "PD", "AP",
                        "PD", "AP", hstClaveAcceso.get("claveacc"), strFechaAut, spiDato.getId());
                    return true;
                  } else {
                    mensaje = mensaje.equals("") ? "No se envio el email cliente, AUTORIZADO"
                        : ", No se envio el email al cliente, AUTORIZADO";

                    ATECOFFGenerarXmlData.methodActualizarMinoutOffline(conn,
                        autorizado.getDocXML(), hstClaveAcceso.get("claveacc"), mensaje, "PD", "AP",
                        "PD", "AP", hstClaveAcceso.get("claveacc"), strFechaAut, spiDato.getId());
                    return true;
                  }
                } else {
                  if (autorizado.getEstadoespecifico().equals("N")) {
                    ATECOFFGenerarXmlData.methodActualizarGuiEstadoOffline(conn, "PD",
                        spiDato.getId());

                    msg.setType("Error");
                    msg.setMessage(mensaje);
                    msg.setTitle("@Error@");
                    return false;
                  }

                  if (autorizado.getInformacion() != null) {
                    mensaje = autorizado.getMensaje() + " - "
                        + autorizado.getInformacion().replace("'", "");
                  }

                  String strSubject = null;

                  if (cltDato.getAtecfeTipoambiente().equals("1")) {
                    strSubject = "Documento Electrónico Rechazada Ambiente Pruebas";
                  } else {
                    strSubject = "Documento Electrónico Rechazada Ambiente Producción";
                  }

                  String strContenido = "Estimado hay un error en el documento guia de despacho de la empresa "
                      + cltDato.getName() + ": " + spiDato.getDocumentNo() + " con clave de acceso "
                      + hstClaveAcceso.get("claveacc") + "\nEl error es: " + mensaje;

                  if (ATECOFF_Operacion_Auxiliares.enviarCorreo(emailSoporte, strSubject,
                      strContenido, null, null, true)) {

                    ATECOFFGenerarXmlData.methodActualizarMinoutOffline(conn,
                        autorizado.getDocXML(), hstClaveAcceso.get("claveacc"), mensaje, "PD", "RZ",
                        "PD", "RZ", hstClaveAcceso.get("claveacc"), strFechaAut, spiDato.getId());

                    msg.setType("Error");
                    msg.setMessage(mensaje);
                    msg.setTitle("@Error@");
                    return false;
                  } else {
                    ATECOFFGenerarXmlData.methodActualizarMinoutOffline(conn,
                        autorizado.getDocXML(), hstClaveAcceso.get("claveacc"), mensaje, "PD", "PD",
                        "PD", "PD", hstClaveAcceso.get("claveacc"), strFechaAut, spiDato.getId());
                    return false;
                  }
                }
              } else {

                String strSubject = null;

                if (cltDato.getAtecfeTipoambiente().equals("1")) {
                  strSubject = "Documento Electrónico Rechazada Ambiente Pruebas";
                } else {
                  strSubject = "Documento Electrónico Rechazada Ambiente Producción";
                }

                String strContenido = "Estimado hay un error en el documento factura de la empresa "
                    + cltDato.getName() + ": " + spiDato.getDocumentNo() + " con clave de acceso "
                    + hstClaveAcceso.get("claveacc") + "\nEl error es: " + mensaje;

                if (ATECOFF_Operacion_Auxiliares.enviarCorreo(emailSoporte, strSubject,
                    strContenido, null, null, true)) {

                  ATECOFFGenerarXmlData.methodActualizarInvoOffline(conn, null,
                      hstClaveAcceso.get("claveacc"), mensaje, "PD", "RZ", "PD", "RZ",
                      hstClaveAcceso.get("claveacc"), strFechaAut, strFechaAut, spiDato.getId());

                  msg.setType("Error");
                  msg.setMessage(mensaje);
                  msg.setTitle("@Error@");
                  return false;
                } else {
                  ATECOFFGenerarXmlData.methodActualizarInvoOffline(conn, null,
                      hstClaveAcceso.get("claveacc"), mensaje, "PD", "PD", "PD", "PD",
                      hstClaveAcceso.get("claveacc"), strFechaAut, strFechaAut, spiDato.getId());
                  return false;
                }
              }
            } else {
              Calendar cldFecha = new GregorianCalendar();
              String strFechaAut = "";

              strFechaAut = sdfFormato.format(cldFecha.getTime());

              ATECOFFGenerarXmlData.methodActualizarMinoutOffline(conn, null,
                  hstClaveAcceso.get("claveacc"), null, "PD", "PD", "PD", "PD",
                  hstClaveAcceso.get("claveacc"), strFechaAut, spiDato.getId());

              msg.setType("Success");
              msg.setTitle("Mensaje");
              msg.setMessage("Documento Procesado Electrónicamente");
              flXml.delete();

              return true;
            }
          } else {
            return false;
          }

        } else {
          return false;
        }
      }

      msg.setType("Error");
      msg.setMessage("No hay un tercero en la retención");
      msg.setTitle("@Error@");
      return false;
    } finally {
      flXml.delete();
      flXml = null;

      fileString = null;

      docXML.clearContent();
      docXML = null;
      ofFormat = null;

      dctDato = null;
      cltDato = null;
      oriDato = null;
      bspDato = null;
      bspDatoDest = null;
      shcDato = null;
      orgDato = null;

      axmlDirMatriz = null;
      axmlDirec = null;
      strDirPartida = null;
      transp = null;
      strDirecDestino = null;
      axmlDocSustento = null;
      axmlDetalles = null;
      axmlEmail = null;

      hstClaveAcceso.clear();
    }
  }
}
